#ifndef DDS_CREATE_MEMBERSHIP_ENTITIES_H
#define DDS_CREATE_MEMBERSHIP_ENTITIES_H

dds_entity_t create_membership_topic(dds_entity_t participant);
dds_entity_t create_membership_reader(dds_entity_t participant, dds_entity_t topic, dds_listener_t* listener);
dds_entity_t create_membership_writer(dds_entity_t participant, dds_entity_t topic);

#endif