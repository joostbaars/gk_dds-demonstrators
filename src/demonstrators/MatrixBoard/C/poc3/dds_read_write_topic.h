#ifndef DDS_READ_WRITE_TOPIC_H
#define DDS_READ_WRITE_TOPIC_H

/* Define number of Samples read. */
#define MAX_SAMPLES 1

void write_topic(dds_entity_t writer, int MBC_id, char topic_id[]);
void read_topic(dds_entity_t reader, int MBC_id);

#endif