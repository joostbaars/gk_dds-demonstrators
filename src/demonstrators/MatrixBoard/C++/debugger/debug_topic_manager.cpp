#include "debug_topic_manager.hpp"

#include "debug_topic_dds.hpp"
#include "general/logger.hpp"

namespace Debug {

// Initialize the static parameters of the DebugTopicManager class
char DebugTopicManager::_buffer[30];
std::unique_ptr<DebugTopicDDS> DebugTopicManager::_pDebugTopicDDS{};
DebugData_DebugInfo DebugTopicManager::_message;

/**
 * @brief initializes the debug topic
 *
 * @param participant the participant of the DDS instance
 * @note This function MUST be executed once for the debugger to work!
 */
void DebugTopicManager::initialize(dds_entity_t *participant) {
    _pDebugTopicDDS = std::make_unique<DebugTopicDDS>(participant, "DebugTopic");
    _pDebugTopicDDS->initialize(true, false);
}

/**
 * @brief sends the connect time to the debug topic
 * This function must be used on a new device that registers
 *
 */
void DebugTopicManager::sendConnectTime(const unsigned int topicId) { sendDebug(MessageTypes::CONNECTING, topicId); }

/**
 * @brief sends that the device is registered to the debug topic
 * This function must be used on an existing device when it notices that a new device was added
 */
void DebugTopicManager::sendDeviceRegistered(const unsigned int topicId) {
    sendDebug(MessageTypes::DEVICE_CONNECTED, topicId);
}

/**
 * @brief sends that the device is registered to the debug topic
 * This function must be used on an existing device when it notices that a new device was added
 */
void DebugTopicManager::sendDeviceRegistered(const unsigned int topicId, const std::string &customTimestamp) {
    sendDebug(customTimestamp, MessageTypes::DEVICE_CONNECTED, topicId);
}

/**
 * @brief sends that a device has disconnected to the debug topic
 * This function must be used on an existing device when it notices that a device was been removed from the DDS network
 */
void DebugTopicManager::sendDeviceDisconnected(const unsigned int topicId) {
    sendDebug(MessageTypes::DEVICE_DISCONNECTED, topicId);
}

/**
 * @brief sends that a device has disconnected to the debug topic
 * This function must be used on an existing device when it notices that a device was been removed from the DDS network
 */
void DebugTopicManager::sendDeviceDisconnected(const unsigned int topicId, const std::string &customTimestamp) {
    sendDebug(customTimestamp, MessageTypes::DEVICE_DISCONNECTED, topicId);
}

/**
 * @brief sendDebug sends the actual messages towards the debug topic
 *
 * @param messageType the type of the message
 */
void DebugTopicManager::sendDebug(const MessageTypes messageType, const unsigned int topicId) {
    _message.messageType = static_cast<int>(messageType);
    std::string timestamp = Logger::currentTimeStampAccurate();
    timestamp.copy(_buffer, 30, 0);
    _buffer[timestamp.size()] = '\0';
    _message.topicId = topicId;
    _message.datetime = _buffer;
    _pDebugTopicDDS->write(_message);
}

/**
 * @brief sendDebug sends the actual messages towards the debug topic
 *
 * @param messageType the type of the message
 */
void DebugTopicManager::sendDebug(const std::string &customTimestamp, const MessageTypes messageType,
                                  const unsigned int topicId) {
    _message.messageType = static_cast<int>(messageType);
    customTimestamp.copy(_buffer, 30, 0);
    _buffer[customTimestamp.size()] = '\0';
    _message.topicId = topicId;
    _message.datetime = _buffer;
    _pDebugTopicDDS->write(_message);
}

} // namespace Debug