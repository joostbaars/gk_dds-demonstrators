#ifndef RUN_APPLICATION_HPP
#define RUN_APPLICATION_HPP

#include <string>

/**
 * @brief Runs a matrix board application on a certain location
 * This class can also stop the matrix board application and read the messages
 * that are normally send to stdout (if readFromMatrixboard is enabled).
 *
 */
class RunApplication {
  public:
    explicit RunApplication(const bool enableRead = false);
    ~RunApplication() = default;

    int runMatrixBoard(const std::string &applicationLocation, const unsigned int id);
    std::string readFromMatrixBoard();
    void stopMatrixBoard() const;

    unsigned int getPID() const { return static_cast<unsigned>(_pidApplication); };

  private:
    pid_t _pidApplication;
    int _fd[2];
    bool _enablePipes;

    void createPipe();
    void configurePipesChild();
    void configurePipesParent();
};

#endif