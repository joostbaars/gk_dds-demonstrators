#ifndef PERFORMANCE_APPLICATION_HPP
#define PERFORMANCE_APPLICATION_HPP

#include "application_topic_dds.hpp"
#include <dds/dds.h>
#include <string>

namespace Performance {

/**
 * @brief An application can be send/read towards the application topic using this class
 *
 */
class ConfigurationSynchronizer {
  public:
    explicit ConfigurationSynchronizer(dds_entity_t *participant);
    ~ConfigurationSynchronizer() = default;

    void initialize() { _applicationTopic.initialize(); }

    void send(const std::string &applicationName);
    bool receivedNewApplication();
    std::string getNewApplication();

  private:
    ApplicationTopicDDS _applicationTopic;
};

} // namespace Performance
#endif