#ifndef TIMESTAMP_DIFFERENCE_HPP
#define TIMESTAMP_DIFFERENCE_HPP

#include <string>

/**
 * @brief calculates the difference between two timestamps
 * 
 */
class TimeStampDifference {
  public:
    static int64_t timeDifference(const std::string &timestampBegin, const std::string &timestampEnd);

  private:
    struct TimeSeparated;

    static void interpunctionToSpace(std::string &inout);
    static TimeSeparated getTimeSeparated(const std::string &timestamp);
    static int32_t getMilliseconds(const TimeSeparated time);
    static int64_t getMicroseconds(const TimeSeparated time);

    struct TimeSeparated {
        int hours;
        int minutes;
        int seconds;
        int milliseconds;
    };
};

#endif