#include "timestampdifference.hpp"

#include <algorithm>
#include <sstream>

/**
 * @brief this function calculates the difference between two timestamps
 * The timestamps need to have the format: "0000-00-00 12:00:00.000000"
 *
 * @param timestampBegin the timestamp measured at the start
 * @param timestampEnd the timestamp measured at the end
 * @return int64_t the difference between the two timestamps in microseconds
 */
int64_t TimeStampDifference::timeDifference(const std::string &timestampBegin, const std::string &timestampEnd) {
    std::string time = timestampBegin.substr(11, timestampBegin.size());
    interpunctionToSpace(time);
    TimeSeparated timeStructBegin = getTimeSeparated(time);

    time = timestampEnd.substr(11, timestampEnd.size());
    interpunctionToSpace(time);
    TimeSeparated timeStructEnd = getTimeSeparated(time);

    if (timeStructBegin.hours == 23 && timeStructEnd.hours == 0) {
        timeStructEnd.hours = 24;
    }
    int64_t microsecondsBegin = getMicroseconds(timeStructBegin);
    int64_t microsecondsEnd = getMicroseconds(timeStructEnd);

    return microsecondsEnd - microsecondsBegin;
}

/**
 * @brief converts interpunction to space character
 *
 * @param inout the string from which the interpunction is replaced by spacebar
 */
void TimeStampDifference::interpunctionToSpace(std::string &inout) {
    std::replace_if(
        inout.begin(), inout.end(), [](const char &c) { return std::ispunct(c); }, ' ');
}

/**
 * @brief returns the separated timestamp
 *
 * @param timestamp the timestamp that needs to be separated
 * @return TimeStampDifference::TimeSeparated separated timestamp
 */
TimeStampDifference::TimeSeparated TimeStampDifference::getTimeSeparated(const std::string &timestamp) {
    std::stringstream ss;
    TimeSeparated returnTime;

    ss << timestamp;
    ss >> returnTime.hours;
    ss >> returnTime.minutes;
    ss >> returnTime.seconds;
    ss >> returnTime.milliseconds;

    return returnTime;
}

/**
 * @brief returns the milliseconds of a timestamp
 *
 * @param time the separated time
 * @return int32_t the milliseconds of a timestamp
 */
int32_t TimeStampDifference::getMilliseconds(const TimeSeparated time) {
    int32_t milliseconds = 0;
    milliseconds = time.hours * 60 * 60 * 1000;
    milliseconds += time.minutes * 60 * 1000;
    milliseconds += time.seconds * 1000;
    milliseconds += time.milliseconds;
    return milliseconds;
}

/**
 * @brief returns the microseconds of a timestamp
 *
 * @param time the separated time
 * @return int32_t the microseconds of a timestamp
 */
int64_t TimeStampDifference::getMicroseconds(const TimeSeparated time) {
    int64_t microseconds = 0;
    microseconds = static_cast<int64_t>(time.hours) * 60 * 60 * 1000 * 1000;
    microseconds += static_cast<int64_t>(time.minutes) * 60 * 1000 * 1000;
    microseconds += static_cast<int64_t>(time.seconds) * 1000 * 1000;
    microseconds += static_cast<int64_t>(time.milliseconds);
    return microseconds;
}