#ifndef DEBUG_TOPIC_PROCESSOR_HPP
#define DEBUG_TOPIC_PROCESSOR_HPP

#include <string>
#include <unordered_map>

#include "MBCData.h"
#include "debugger/debug_message_types.hpp"
#include "debugger/debug_topic_dds.hpp"
#include "general/measurements_buffer.hpp"

namespace Debug {

/**
 * @brief Contains the implementation for processing the messages from the debug topic
 */
class DebugTopicProcessor {
  public:
    struct ReceivedMsg {
        std::vector<std::string> first;
        std::vector<std::string> second; // More active matrix boards mean more secondary values
    };

    // Constructor & destructor
    DebugTopicProcessor(dds_entity_t *participant, const unsigned int id);
    ~DebugTopicProcessor() = default;

    void initialize();

    void checkDebugTopic();

    /**
     * @brief Returns the connected time data
     *
     * @return ReceivedMsg a struct containing the connect data
     */
    ReceivedMsg getConnected() const { return _receivedConnected; };
    /**
     * @brief Returns the disconnected time data
     *
     * @return ReceivedMsg a struct containing the disconnected data
     */
    ReceivedMsg getDisconnected() const { return _receivedDisconnected; };
    void setDisconnected(const std::string &datetime);

    void reset();
    void resetDisconnected();

  private:
    const unsigned int _id;
    bool _isConnecting, _isDisconnecting;
    ReceivedMsg _receivedConnected;
    ReceivedMsg _receivedDisconnected;
    DebugTopicDDS _debugTopic;
};

} // namespace Debug
#endif // SPECIFIC_MATRIXBOARD_H