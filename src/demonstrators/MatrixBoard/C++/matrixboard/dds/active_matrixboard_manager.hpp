#ifndef ACTIVE_MATRIX_BOARDS_H
#define ACTIVE_MATRIX_BOARDS_H

#include <dds/dds.h>
#include <map>

#include "matrixboard/dds/dds_active_matrixboard.hpp"
#include "matrixboard/dds/subsequent_matrixboards.hpp"

namespace matrixboard {

/**
 * @brief Manages the shared matrix board topic for the active matrix boards
 *
 * This class communicates on the shared matrix boards topic and has a function
 * for requesting the next 2 boards / ID's. The registerToTopic function should
 * be executed once. checkChanges() must be executed regularly to check for updates
 * on the shared topic. getSubsequentBoards() can be executed after checkChanges() for the
 * most recent boards.
 */
class ActiveMatrixBoardManager {
  public:
    // Constructor & Destructor
    ActiveMatrixBoardManager(dds_entity_t *participant, const unsigned int id, const std::string &topic);
    ~ActiveMatrixBoardManager() = default;

    // Initialization
    void initialize();

    // Update functions
    bool checkChanges();
    SubsequentMatrixBoards getNextBoards() const { return _subsequentMatrixBoards; }

  private:
    using MatrixboardsMap = std::multimap<unsigned int, dds_instance_handle_t>;
    MatrixboardsMap _sharedMatrixBoards;
    SubsequentMatrixBoards _subsequentMatrixBoards;

    const unsigned int _id;
    const std::string _baseTopicName;

    DDSActiveMatrixBoard _activeMB;

    void removeMatrixBoard(const dds_instance_handle_t handle);
    void addMatrixBoards();
    bool updateBoards();

    inline void writeOwnID();
};

/**
 * @brief writes its own ID to the topic as well as the map
 *
 */
inline void ActiveMatrixBoardManager::writeOwnID() {
    _activeMB.writeUserID(_id);
    _sharedMatrixBoards.insert(std::make_pair(_id, 0));
}

} // namespace matrixboard
#endif