#ifndef DDS_ACTIVE_MATRIX_BOARD_H
#define DDS_ACTIVE_MATRIX_BOARD_H

#include <atomic>
#include <dds/dds.h>
#include <string>

#include "dds/dds_basics.hpp"

namespace matrixboard {

/**
 * @brief Manages the shared matrix board topic for the active matrix boards
 *
 * This class communicates on the shared matrix boards topic and has a function
 * for requesting the next 2 boards / ID's. The registerToTopic function should
 * be executed once. checkChanges() must be executed regularly to check for updates
 * on the shared topic. getSubsequentBoards() can be executed after checkChanges() for the
 * most recent boards.
 */
class DDSActiveMatrixBoard : public DDSBasics<ActiveMBData_MBCid> {
  public:
    // Constructor & Destructor
    explicit DDSActiveMatrixBoard(dds_entity_t *participant);
    ~DDSActiveMatrixBoard() = default;

    struct MessageInfo {
        bool newMessage = false;
        unsigned int id;
        dds_instance_handle_t handle;
    };

    MessageInfo readMessage();
    void writeUserID(const unsigned int id);
    dds_liveliness_changed_status_t getStatus() const;

    bool receivedNewMessage();

  private:
    std::atomic_bool _receivedNewMessage;

    dds_listener_t *createListener();
    void configureQoS(dds_qos_t *qos);
    void configureListener(dds_listener_t *listener);
    static void newMessageCallback(dds_entity_t reader, void *arg);
};

} // namespace matrixboard
#endif