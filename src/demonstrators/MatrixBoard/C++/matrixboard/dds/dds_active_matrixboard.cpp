#include "dds_active_matrixboard.hpp"

#include "MBCData.h"

namespace matrixboard {

/**
 * @brief Constructs a new DDSActiveMatrixBoard object
 *
 * @param participant the DDS participant that this class is going to use for the DDS communication
 * @param id the ID of the matrix board
 * @param topic the topic of the DDSActiveMatrixBoard shared topic
 */
DDSActiveMatrixBoard::DDSActiveMatrixBoard(dds_entity_t *participant)
    : DDSBasics<ActiveMBData_MBCid>{participant, ActiveMBData_MBCid_desc}, _receivedNewMessage{false} {}

/**
 * @brief configures the necessary QoS policies for the DDSActiveMatrixBoard class
 *
 * @param qos the qos object
 */
void DDSActiveMatrixBoard::configureQoS(dds_qos_t *qos) {
    dds_qset_durability(qos, DDS_DURABILITY_TRANSIENT_LOCAL);
    dds_qset_history(qos, DDS_HISTORY_KEEP_LAST, 5);
    dds_qset_liveliness(qos, DDS_LIVELINESS_AUTOMATIC, DDS_SECS(5));
    // May only be reliable, I dont know why it may not be best effort
    dds_qset_reliability(qos, DDS_RELIABILITY_RELIABLE, DDS_SECS(10));
}

/**
 * @brief creates a custom listener for adding the matrix board ID and isWriter information
 *
 * @return dds_listener_t* the created listener
 */
dds_listener_t *DDSActiveMatrixBoard::createListener() { return dds_create_listener(&_receivedNewMessage); }

/**
 * @brief contains the configuration of the listener
 *
 * @param listener the listener object
 */
void DDSActiveMatrixBoard::configureListener(dds_listener_t *listener) {
    dds_lset_liveliness_changed(listener, NULL);
    dds_lset_data_available(listener, newMessageCallback);
}

/**
 * @brief writes the own ID to the shared topic as well as its own map
 *
 */
void DDSActiveMatrixBoard::writeUserID(const unsigned int id) {
    ActiveMBData_MBCid msg;
    msg.userID = static_cast<int32_t>(id);

    write(msg);
}

/**
 * @brief reads a new message from the active matrix boards topic
 *
 * @return MessageInfo the information of the message
 */
DDSActiveMatrixBoard::MessageInfo DDSActiveMatrixBoard::readMessage() {
    MessageInfo receivedMessage;

    dds_sample_info_t infos[1];
    dds_return_t returnValue;
    do {
        returnValue = dds_take(_reader, _allocatedMsg, infos, 1, 1);
        ActiveMBData_MBCid *msg = static_cast<ActiveMBData_MBCid *>(_allocatedMsg[0]);
        if (infos[0].valid_data && returnValue > 0) {
            receivedMessage.newMessage = true;
            receivedMessage.id = static_cast<unsigned>(msg->userID);
            receivedMessage.handle = infos[0].publication_handle;
        }
    } while (returnValue != 0 && receivedMessage.newMessage == false);

    return receivedMessage;
}

/**
 * @brief gets the status of the liveliness on the active matrix boards topic
 *
 * @return dds_liveliness_changed_status_t the status of the liveliness
 */
dds_liveliness_changed_status_t DDSActiveMatrixBoard::getStatus() const {
    dds_liveliness_changed_status_t livelinessStatus;
    dds_get_liveliness_changed_status(_reader, &livelinessStatus);
    return livelinessStatus;
}

/**
 * @brief the callback for when a new message is received
 *
 * @param reader the reader at which the callback is executed
 * @param arg an atomic_bool that defines when a new message is received
 */
void DDSActiveMatrixBoard::newMessageCallback(dds_entity_t reader, void *arg) {
    std::atomic_bool *receivedMsg = static_cast<std::atomic_bool *>(arg);
    *receivedMsg = true;
}

/**
 * @brief returns if a new message has been received
 *
 * @return true a new message has been received
 * @return false no message is received
 */
bool DDSActiveMatrixBoard::receivedNewMessage() {
    if (_receivedNewMessage) {
        _receivedNewMessage = false;
        return true;
    }
    return false;
}

} // namespace matrixboard