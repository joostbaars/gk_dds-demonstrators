#ifndef SUBSEQUENT_MATRIXBOARDS_H
#define SUBSEQUENT_MATRIXBOARDS_H

namespace matrixboard {
/**
 * @brief structure for storing / returning the next matrix boards
 *
 */
struct SubsequentMatrixBoards {
    unsigned int subsequent = 0;
    unsigned int afterSubsequent = 0;
    inline bool operator==(const SubsequentMatrixBoards &other) const {
        return (subsequent == other.subsequent && afterSubsequent == other.afterSubsequent);
    }
};
} // namespace matrixboard

#endif