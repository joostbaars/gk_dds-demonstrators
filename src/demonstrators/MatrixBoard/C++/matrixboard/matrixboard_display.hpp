#ifndef MATRIXBOARD_DISPLAY_H
#define MATRIXBOARD_DISPLAY_H

namespace matrixboard {

/**
 * @brief Contains the implementation / simulation of the display of the matrix board
 * This class contains the simulation of the display of the matrix board. The matrix board can be set
 * to a certain value. This value can also be requested.
 */
class MatrixBoardDisplay {
  public:
    explicit MatrixBoardDisplay(const unsigned int maxMatrixboardValue = 999);
    ~MatrixBoardDisplay() = default;

    unsigned int getDisplayValue() const;
    void setDisplayValue(const unsigned int displayValue);

  private:
    unsigned int displayValue_;
    unsigned int maxMatrixboardValue_;
};

} // namespace matrixboard

#endif // MATRIXBOARD_DISPLAY_H