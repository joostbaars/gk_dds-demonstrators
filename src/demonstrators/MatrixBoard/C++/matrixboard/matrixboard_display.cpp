#include "matrixboard_display.hpp"

#include "general/logger.hpp"

namespace matrixboard {

/**
 * @brief Construct a new Matrix Board Display:: Matrix Board Display object
 *
 * @param maxMatrixboardValue the maximum value the matrixboard can show
 */
MatrixBoardDisplay::MatrixBoardDisplay(const unsigned int maxMatrixboardValue)
    : displayValue_{120}, maxMatrixboardValue_{maxMatrixboardValue} {}

/**
 * @brief getDisplayValue returns the speed "shown" on the matrixboard
 *
 * @return unsigned int the speed on the matrixboard
 */
unsigned int MatrixBoardDisplay::getDisplayValue() const { return displayValue_; }

/**
 * @brief setDisplayValue sets the number that is "shown" on the matrixboard
 *
 * @param displayValue the value that is set on the matrixboard
 */
void MatrixBoardDisplay::setDisplayValue(const unsigned int displayValue) {
    // Maximum number that can be written on matrixboards
    if (displayValue_ > maxMatrixboardValue_) {
        return;
    }
    // Display new speed to the user(s)
    if (displayValue != displayValue_) {
         Logger::print(Logger::LogLevel::INFO, "New speed: ", displayValue);
    }
    displayValue_ = displayValue;
}

} // namespace matrixboard