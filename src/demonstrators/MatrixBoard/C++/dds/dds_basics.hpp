#ifndef DDS_BASICS_H
#define DDS_BASICS_H

#include "MBCData.h"
#include <dds/dds.h>

#include "general/helper_functions.hpp"
#include <chrono>
#include <thread>

#include "general/logger.hpp"

#include "debugger/debug_topic_manager.hpp"

/**
 * @brief DDSBasics is an abstract class for communicating with DDS
 * This class is a base class.
 * The DDS objects are reachable by the derived class, this gives the derived class 100% control
 * of the DDS protocol. This class only contains basic DDS functions (write and read). More
 * functionalities can be added in the derived class
 *
 * @tparam MsgType the struct of the message that is send / received
 */
template <typename MsgType>
class DDSBasics {
  public:
    DDSBasics(dds_entity_t *participant, const dds_topic_descriptor_t descriptor);
    virtual ~DDSBasics();

    virtual void registerToTopic(const std::string &topicName, const bool createWriter = true,
                                 const bool createReader = true);
    void write(MsgType data);

    MsgType getLastMessage() const { return _receivedMessage; };
    bool checkNewData();

  protected:
    // Functions for custom configuration of the QoS policies
    virtual void configureQoS(dds_qos_t *qos);
    virtual void configureListener(dds_listener_t *listener);
    virtual dds_listener_t *createListener();

    using milliseconds = int;
    static constexpr milliseconds _ddsInitialization = 25;
    MsgType _receivedMessage;
    dds_entity_t _reader, _writer;
    dds_entity_t _topic;
    dds_topic_descriptor_t _descriptor;
    dds_entity_t *_participant;
    void *_allocatedMsg[1];

    static void checkDDSerror(const int32_t value);
};

/**
 * @brief creates the object and allocates necessary memory
 *
 * @param descriptor a descriptor of the messages that are send / received
 */
template <typename MsgType>
DDSBasics<MsgType>::DDSBasics(dds_entity_t *participant, const dds_topic_descriptor_t descriptor)
    : _descriptor{descriptor}, _participant{participant} {
    _allocatedMsg[0] = dds_alloc(sizeof(MsgType));
}

/**
 * @brief Destroys the DDSBasics object
 *
 */
template <typename MsgType>
DDSBasics<MsgType>::~DDSBasics() {
    dds_sample_free(_allocatedMsg[0], &_descriptor, DDS_FREE_ALL);
}

/**
 * @brief registers to a topic
 *
 * @note This function should only be executed once!
 */
template <typename MsgType>
void DDSBasics<MsgType>::registerToTopic(const std::string &topicName, const bool createWriter,
                                         const bool createReader) {
    _topic = dds_create_topic(*_participant, &_descriptor, topicName.c_str(), NULL, NULL);
    checkDDSerror(_topic);
    dds_listener_t *listener = createListener();
    dds_qos_t *qos = dds_create_qos();

    configureListener(listener);
    configureQoS(qos);

    if (createWriter) {
        _writer = dds_create_writer(*_participant, _topic, qos, NULL);
        checkDDSerror(_writer);
    }
    if (createReader) {
        _reader = dds_create_reader(*_participant, _topic, qos, listener);
        checkDDSerror(_reader);
    }
    dds_delete_qos(qos);
    dds_delete_listener(listener);
    std::this_thread::sleep_for(std::chrono::milliseconds(_ddsInitialization));
}

/**
 * @brief Writes data to the topic that it registered to with registerToTopic()
 *
 * @param data the data that is written to the topic
 * @note registerToTopic() should always be executed first on the object before executing this function!
 */
template <typename MsgType>
void DDSBasics<MsgType>::write(MsgType data) {
    dds_write(_writer, &data);
}

/**
 * @brief checks for new data on the topic it is registered to
 *
 * @return true New data is received and can be read by executing the function lastMessage()
 * @return false No new data was received
 *
 * @note The registerToTopic() function should always be executed first on the object!
 */
template <typename MsgType>
bool DDSBasics<MsgType>::checkNewData() {
    dds_sample_info_t infos[1];
    dds_return_t returnValue = dds_take(_reader, _allocatedMsg, infos, 1, 1);
    MsgType *msg = static_cast<MsgType *>(_allocatedMsg[0]);
    if (infos[0].valid_data && returnValue > 0) {
        _receivedMessage = *msg;
        return 1;
    }
    return 0;
}

/**
 * @brief Checks for a DDS error and throws an error upon failure
 *
 * @param value the returned value
 */
template <typename MsgType>
void DDSBasics<MsgType>::checkDDSerror(const int32_t value) {
    if (value < 0) {
        throw std::runtime_error(HelperFunctions::argToString("Matrixboard: ", __func__, ": ", dds_strretcode(-value)));
    }
}

/**
 * @brief configures the QoS policies. By default, no QoS policies are configured.
 *
 * @note this function should be overloaded for custom QoS policies
 */
template <typename MsgType>
void DDSBasics<MsgType>::configureQoS(dds_qos_t *qos) {
    ;
}

/**
 * @brief configures the listener. By default, no listener is configured.
 *
 * @note this function should be overloaded for configuring a listener
 */
template <typename MsgType>
void DDSBasics<MsgType>::configureListener(dds_listener_t *listener) {
    ;
}

/**
 * @brief creates a listener. By default, no arguments are given to this listener.
 *
 * @note this function should be overloaded for configuring a listener with custom arguments
 */
template <typename MsgType>
dds_listener_t *DDSBasics<MsgType>::createListener() {
    return dds_create_listener(NULL);
}

#endif