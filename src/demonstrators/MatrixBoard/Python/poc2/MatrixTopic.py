import cdds as dds

class MatrixTopic(dds.FlexyTopic):
    def __init__(self, dp, name, keygen=None, qos=None):
        self.topic_name = name
        super().__init__(dp, name)

    def get_name():
        return topic_name
