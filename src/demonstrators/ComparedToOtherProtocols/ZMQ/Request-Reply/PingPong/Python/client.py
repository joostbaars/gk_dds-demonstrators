import zmq

context = zmq.Context()

print("connecting to hello world server")


socket = context.socket(zmq.REQ) # set this node to request data
socket.connect("tcp://localhost:5555") # request on port 5555 on localhost

for request in range(10):
    print("Sending request %s ,," % request)
    socket.send(b"hello")
    
    message = socket.recv()
    print("received reply %s [ %s ]" % (request, message))