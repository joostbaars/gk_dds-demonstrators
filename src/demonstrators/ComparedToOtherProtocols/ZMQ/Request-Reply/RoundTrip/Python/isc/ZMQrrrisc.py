import zmq 
import sys
from time import sleep
from datetime import datetime

import logging
logging.basicConfig(level=logging.INFO)
from logging import getLogger
logger = getLogger(__name__)

ReceiveUrl=         sys.argv[1]     if len(sys.argv) > 1 else "tcp://*:5555"
SendUrl=            sys.argv[2]     if len(sys.argv) > 2 else "tcp://localhost:5555"
IsMaster=           sys.argv[3]     if len(sys.argv) > 3 else 0


if IsMaster:
    logger.info("master started")
else:
    logger.info("hub started")

logger.info("Receiving on: %s" % ReceiveUrl)
logger.info("Sending on: %s" % SendUrl)

NumberOfMessagesToSend = 10000

if IsMaster:
    
    context = zmq.Context()
    socket = context.socket(zmq.REQ) # set this node to request data
    socket.connect("tcp://localhost:5555") # request on port 5555 on localhost
    for request in range(10):
        print("Sending request %s ,," % request)
        socket.send(b"hello")
        
        message = socket.recv()
        print("received reply %s [ %s ]" % (request, message))
else:
    
    context = zmq.Context() 
    socket = context.socket(zmq.REP) # set this as a replying node/hub/
    socket.bind("tcp://*:5555") # receive and reply on port 5555
    while True:
        message = socket.recv()
        print("received request %s" % message)
        
        sleep(1)
        
        socket.send(b"world") # reply to request
    
    
