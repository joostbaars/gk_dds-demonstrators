#include "matrixboard.hpp"
#include <iostream>
#include <string>

std::string getMatrixBoardIdFromUser();

int main(int argc, const char **argv) {
    std::string id;
    id = getMatrixBoardIdFromUser();
    if (std::stoi(id) < 10) {
        MatrixBoard board(id, "tcp://127.0.0.1:557", "tcp://127.0.0.1:5550", "tcp://127.0.0.1:5551");
        try {
            board.run();
        } catch (const std::exception &e) {
            std::cerr << "Error catched in main: " << e.what();
            return -1;
        }
    } else {
        std::cout << "number: " << id << " not under 10" << std::endl;
    }
    return 0;
}

std::string getMatrixBoardIdFromUser() {
    std::string id;
    std::cout << "Type a number: ";                     // Type a number and press enter
    std::cin >> id;                                     // Get user input from the keyboard
    std::cout << "Your number is: " << id << std::endl; // Display the input value
    return id;
}
