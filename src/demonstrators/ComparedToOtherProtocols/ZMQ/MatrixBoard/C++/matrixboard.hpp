#ifndef MATRIXBOARD_H
#define MATRIXBOARD_H

#include "car_sensor.hpp"
#include "matrixboard_display.hpp"
#include "performance_measurements.hpp"
#include "measurements_file.hpp"
#include <chrono>
#include <iostream>
#include <map>
#include <thread>
#include <string>
#include <zmq.hpp>

/**
 * @brief MatrixBoardSim contains the functions for the matrixboard simulation
 *
 */
class MatrixBoard {
  public:
    MatrixBoard(std::string id, std::string myAddress, std::string proxyBackendAddress,
                std::string proxyFrontendAddress);
    ~MatrixBoard();
    void run();
    void updateBoardValue(std::string msg_board_id, std::string msg_cars);
    void handleMatrixBoardMessage(std::string msg_str);
    void checkLiveliness(std::chrono::_V2::system_clock::time_point &start1,
                         std::chrono::_V2::system_clock::time_point &start2);
    bool processNewBoard(int id, std::string address);
    zmq::message_t generateMessageFromString(std::string message);
    std::vector<std::string> readTopicAndMessage(zmq::socket_t *_subsocket);
    bool initLiveliness();
    void sendDeadMessage(int dead_board_id);
    void handleDeadBoardMessage(std::string msg_str, std::size_t dead_character_pos);
    void handleNewBoardMessage(std::string msg_str, std::size_t dead_character_pos);
    void handleAliveBoardMessage(std::string msg_str);
    enum vector_pos { TOPIC, 
                      MESSAGE,
                      MESSAGE_ID};

  private:
    std::string _id;
    unsigned int _cars_self;
    const std::string _MY_ADDRESS;
    const std::string _PROXY_SUB_ADDRESS;
    const std::string _PROXY_PUB_ADDRESS;
    zmq::context_t *_context;
    zmq::socket_t *_board_proxy_pub;
    zmq::socket_t *_sub_socket;
    zmq::socket_t *_publisher;
    std::map<int, std::string> _board_addresses;
    std::map<int, int> _board_values;

  protected:
    matrixboard::MatrixBoardDisplay _display;
    matrixboard::CarSensor _sensor;

    typedef std::chrono::high_resolution_clock Clock;

    unsigned int calculateSpeed(const unsigned int traffic);
};

#endif // !MATRIXBOARD_H