#include "proxy.hpp"

/**
 * @brief Construct a new Proxy:: Proxy object
 *
 * inits zmq context and sockets needed for the proxy
 *
 * @param serverAddress
 * @param subPort
 * @param pubPort
 */
Proxy::Proxy(std::string serverAddress, std::string subPort, std::string pubPort)
    : _SERVER_ADDRESS{serverAddress}, _SUB_PORT{subPort}, _PUB_PORT{pubPort}, _context{new zmq::context_t()},
      _publisher{new zmq::socket_t(*_context, ZMQ_XPUB)}, _subscriber{new zmq::socket_t(*_context, ZMQ_XSUB)} {}
/**
 * @brief starts the zmq proxy
 *
 */
void Proxy::start() {
    std::cout << "proxy sub:"
              << "tcp://127.0.0.1:" + _SUB_PORT << std::endl;
    std::cout << "proxy pub:"
              << "tcp://127.0.0.1:" + _PUB_PORT << std::endl;
    _subscriber->bind("tcp://127.0.0.1:" + _SUB_PORT);
    _publisher->bind("tcp://127.0.0.1:" + _PUB_PORT);
    zmq::proxy(*_subscriber, *_publisher);
}