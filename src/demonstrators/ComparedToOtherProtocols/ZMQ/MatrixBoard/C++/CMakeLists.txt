cmake_minimum_required(VERSION 3.10)

find_package(cppzmq REQUIRED)

add_executable(zmqmatrixboard main.cpp matrixboard.cpp car_sensor.cpp matrixboard_display.cpp performance_measurements.cpp 
measurements_file.cpp)

target_link_libraries(zmqmatrixboard cppzmq)

add_executable(matrixboardproxy mainproxy.cpp proxy.cpp)

target_link_libraries(matrixboardproxy cppzmq)

add_executable(measurementsfiles main_measurements_file.cpp measurements_file.cpp) 