#include "matrixboard.hpp"

/**
 * @brief Construct a new Matrix Board:: Matrix Board object
 *
 * Initiates matrixboard objects
 * Creates needed zmq context and sockets
 *
 * @param id matrixboard id
 * @param myAddress network adress and port, last number of port is id
 * @param proxySubAddress network address and port of proxy sub
 * @param proxyPubAddress network address and port of proxy pub
 */
MatrixBoard::MatrixBoard(std::string id, std::string myAddress, std::string proxySubAddress,
                         std::string proxyPubAddress)
    : _MY_ADDRESS{myAddress + id}, _PROXY_SUB_ADDRESS{proxySubAddress}, _PROXY_PUB_ADDRESS{proxyPubAddress}, _id{id},
      _context{new zmq::context_t()}, _publisher{new zmq::socket_t(*_context, ZMQ_PUB)},
      _sub_socket{new zmq::socket_t(*_context, ZMQ_SUB)}, _board_proxy_pub{new zmq::socket_t(*_context, ZMQ_PUB)} {
    _cars_self = _sensor.countedCars();
    _sub_socket->setsockopt(ZMQ_SUBSCRIBE, "b", 1);
    std::cout << _id << " publish to:" << _MY_ADDRESS << std::endl;
    _publisher->bind(_MY_ADDRESS);
    std::cout << "connect to proxy sub:" << _PROXY_SUB_ADDRESS << std::endl;
    _board_proxy_pub->connect(_PROXY_PUB_ADDRESS);
    std::cout << "connect to proxy pub:" << _PROXY_PUB_ADDRESS << std::endl;
    _sub_socket->connect(_PROXY_SUB_ADDRESS);
}

/**
 * @brief Destroy the Matrix Board:: Matrix Board object
 *
 */
MatrixBoard::~MatrixBoard() { _context->close(); }
/**
 * @brief Run function contains logic to run a matrixboard sim
 *
 * inits board liveliness
 * sends out amount of cars
 * polls subbed boards
 * handles messages from subbed boards
 *
 */
void MatrixBoard::run() {
    std::cout << "run" << std::endl;
    initLiveliness();
    zmq::pollitem_t items[] = {{*_sub_socket, 0, ZMQ_POLLIN, 0}};
    std::cout << "start loop" << std::endl;
    auto start1 = Clock::now();
    auto start2 = Clock::now();
    int message_id = 0;
    PerformanceMeasurements sendperf;
    PerformanceMeasurements recperf;
    MeasurementsFile performanceFile("measurements.csv");
    sendperf.start();
    while (true) {
        //std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        _cars_self = _sensor.countedCars();
        _publisher->send(generateMessageFromString(_id), zmq::send_flags::sndmore);
        _publisher->send(generateMessageFromString(std::to_string(_cars_self)), zmq::send_flags::sndmore);
        _publisher->send(generateMessageFromString(std::to_string(message_id)), zmq::send_flags::none);
        sendperf.start();
        std::vector<std::string> measurements{std::to_string(message_id), _id, "-1", _id, std::to_string(_cars_self), sendperf.getStartMili()};
        performanceFile.write(measurements);
        message_id++;
        zmq::poll(&items[0], 1, 1);
        if (items[0].revents & ZMQ_POLLIN) {
            std::vector<std::string> msg= readTopicAndMessage(_sub_socket);
            if (msg[TOPIC] == "b") {
                recperf.start();
                std::vector<std::string> recmeasurements{msg[MESSAGE], "matrixboard", _id, msg[TOPIC], msg[MESSAGE], recperf.getStartMili()};
                handleMatrixBoardMessage(msg[MESSAGE]);
            } else {
                recperf.start();
                std::vector<std::string> recmeasurements{msg[MESSAGE_ID], msg[TOPIC], _id, msg[TOPIC], msg[MESSAGE], recperf.getStartMili()};
                performanceFile.write(recmeasurements);
                updateBoardValue(msg[TOPIC], msg[MESSAGE]);
                if (_board_addresses.size() == 2) {
                    if (std::stoi(msg[TOPIC]) == _board_addresses.begin()->first) {
                        start1 = Clock::now();
                    } else if (std::stoi(msg[TOPIC]) == (--_board_addresses.end())->first) {
                        start2 = Clock::now();
                    }
                } else if (_board_addresses.size() == 1 &&
                           std::stoi(msg[TOPIC]) == _board_addresses.begin()->first) {
                    start1 = Clock::now();
                }
            }
        }
        checkLiveliness(start1, start2);
    }
}
/**
 * @brief Function checks if subbed to boards are still alive by checkin how long ago the last message was
 *
 * If the board hasn't received a message from a subbed to board for over 3 seconds it will let the system know the
 * board is dead.
 *
 * @param start1 time of last message of first board
 * @param start2 time of last message of second board
 */
void MatrixBoard::checkLiveliness(std::chrono::_V2::system_clock::time_point &start1,
                                  std::chrono::_V2::system_clock::time_point &start2) {
    auto end = Clock::now();
    if (_board_addresses.size() == 0) {
        start1 = Clock::now();
        start2 = Clock::now();
    } else if (_board_addresses.size() == 2) {
        if (std::chrono::duration_cast<std::chrono::seconds>(end - start1).count() > 3) {
            std::cout << "dead board 1:" << _board_addresses.begin()->first
                      << " address:" << _board_addresses.begin()->second
                      << " timeout:" << std::chrono::duration_cast<std::chrono::seconds>(end - start1).count()
                      << std::endl;
            int dead = _board_addresses.begin()->first;
            sendDeadMessage(dead);
            start1 = Clock::now();
        }
        if (std::chrono::duration_cast<std::chrono::seconds>(end - start2).count() > 3) {
            std::cout << "dead board 2:" << (--_board_addresses.end())->first
                      << " address:" << (--_board_addresses.end())->second
                      << " timeout:" << std::chrono::duration_cast<std::chrono::seconds>(end - start2).count()
                      << std::endl;
            int dead = (--_board_addresses.end())->first;
            sendDeadMessage(dead);
            start2 = Clock::now();
        }
    } else if (_board_addresses.size() == 1) {
        if (std::chrono::duration_cast<std::chrono::seconds>(end - start1).count() > 3) {
            std::cout << "dead board:" << _board_addresses.begin()->first
                      << " address:" << _board_addresses.begin()->second
                      << " timeout:" << std::chrono::duration_cast<std::chrono::seconds>(end - start1).count()
                      << std::endl;
            int dead = _board_addresses.begin()->first;
            sendDeadMessage(dead);
            start1 = Clock::now();
        }
        start2 = Clock::now();
    }
}
/**
 * @brief Function to turn a string into a zmq message
 *
 * @param message string to be sent
 * @return zmq::message_t zmq message
 */
zmq::message_t MatrixBoard::generateMessageFromString(std::string message) {
    zmq::message_t msg(message.size());
    memcpy(msg.data(), message.c_str(), message.size());
    return msg;
}
/**
 * @brief Function to extract topic and message from a message on a socket.
 *
 * @param _subsocket The socket to get the topic and message from
 * @return std::map<std::string, std::string> map with topic as key and message as value
 */
std::vector<std::string> MatrixBoard::readTopicAndMessage(zmq::socket_t *_subsocket) {
    zmq::message_t topic;
    zmq::message_t msg_id;
    zmq::message_t msg_val;
    _subsocket->recv(topic, zmq::recv_flags::none);
    _subsocket->recv(msg_val, zmq::recv_flags::none);
    std::string topic_str = std::string(static_cast<char *>(topic.data()), topic.size()); 
    std::string msg_val_str = std::string(static_cast<char *>(msg_val.data()), msg_val.size());
    std::vector<std::string> msg{topic_str, msg_val_str};  
    if(topic_str != "b") {
    _subsocket->recv(msg_id, zmq::recv_flags::none);
    std::string msg_id_str = std::string(static_cast<char *>(msg_id.data()), msg_id.size());
    msg.push_back(msg_id_str);
    std::cout << "id:"<< msg_id_str << std::endl;
    } 
    std::cout << "Received '" << msg_val_str << "'"
              << " on '" << topic_str << "'" << std::endl;
    return msg;
}
/**
 * @brief update the value of the amount of cars value of a subbed to board
 *
 * @param msg_board_id id of board to be updated as string from the message
 * @param msg_cars amoun of cars as string from message
 */
void MatrixBoard::updateBoardValue(std::string msg_board_id, std::string msg_cars) {
    int board_id = std::stoi(msg_board_id);
    int cars = std::stoi(msg_cars);
    _board_values[board_id] = cars;
}
/**
 * @brief handle a new message from the matrixboard proxy.
 *
 * Handles a new message of from the matrixboardproxy.
 * Containing ! means the board is dead.
 * Containing ? means the board is new.
 *
 * Dead:
 * If subbed to the dead board erase it and disconnect.
 * Else if the dead board from the message is the board itself, send message that its alive.
 * If de dead boards id is smaller than the id of this board, send a message so boards before the dead board can
 * subscribe to this one.
 *
 * New:
 * Process the new board with processnewboard function and sub if it is required.
 * If the new boards id is smaller than the id of this board, send a message so boards before the dead board can
 * subscribe to this one.
 *
 * Alive:
 * Process the new board with processnewboard function and sub if it is required.
 *
 * @param msg_str string containing the message from the proxy
 */
void MatrixBoard::handleMatrixBoardMessage(std::string msg_str) {
    std::size_t dead = msg_str.find("!");
    std::size_t new_board = msg_str.find("?");
    if (dead != std::string::npos) {
        msg_str.erase(dead);
        handleDeadBoardMessage(msg_str, dead);
    } else if (new_board != std::string::npos) {
        handleNewBoardMessage(msg_str, new_board);
    } else {
        handleAliveBoardMessage(msg_str);
    }
}
/**
 * @brief Function to let the system know this board is alive.
 *
 * This function sends a message to the proxy till it can see the message itself.
 * The ? indicates that this is a new board.
 * Once it receives its own message it knows that the system knows it's alive.
 *
 * @return true
 * @return false
 */
bool MatrixBoard::initLiveliness() {
    std::cout << "init liveliness" << std::endl;
    bool done = false;
    zmq::pollitem_t items[] = {
        {*_sub_socket, 0, ZMQ_POLLIN, 0},
    };
    while (!done) {
        _board_proxy_pub->send(generateMessageFromString("b"), zmq::send_flags::sndmore);
        _board_proxy_pub->send(generateMessageFromString(_id + "?|" + _MY_ADDRESS), zmq::send_flags::none);
        zmq::poll(&items[0], 1, 1);
        if (items[0].revents & ZMQ_POLLIN) {
            std::vector<std::string> msg = readTopicAndMessage(_sub_socket);
            std::string message = msg[MESSAGE];
            std::size_t pos = message.find("?");
            if (pos != std::string::npos) {
                int id = std::stoi(message.substr(0, pos));
                if (id == std::stoi(_id)) {
                    std::cout << "I'm alive:" << message << std::endl;
                    done = true;
                }
            }
        }
    }
    return done;
}
/**
 * @brief Determine if the new board should be subscribed too by this board.
 *
 * Check if the new boards id is smaller than the current second board if there are two boards
 * otherwise check if not subbed to the board allready.
 * In the first case it allready disconnects from the second board.
 *
 * @param new_board The id of the new board.
 * @param address The network address of the new board.
 * @return true board should be subscribed to
 * @return false board should not be subscribed to
 */
bool MatrixBoard::processNewBoard(int new_board, std::string address) {
    if (new_board > std::stoi(_id)) {
        std::cout << "nb:" << new_board << std::endl;
        if (_board_addresses.size() < 2 && _board_addresses.begin()->first != new_board) {
            _board_addresses[new_board] = address;
            return true;
        } else {
            int second = (--_board_addresses.end())->first;
            std::cout << "sec: " << second << " new:" << new_board << std::endl;
            if (new_board < second && _board_addresses.begin()->first != new_board) {
                _board_values[new_board] = 0;
                _board_addresses[new_board] = address;
                _sub_socket->disconnect(_board_addresses[second]);
                std::cout << "disconnected:" << second << " | " << _board_addresses[second]
                          << " new size:" << _board_addresses.size() - 1 << std::endl;
                _board_addresses.erase(second);
                return true;
            }
        }
    }
    return false;
}
/**
 * @brief sends a message to te matrixboard proxy that subbed to board is not responding and is considered dead.
 *
 * @param dead_board_id The id of the board that is considered dead.
 */
void MatrixBoard::sendDeadMessage(int dead_board_id) {
    _sub_socket->disconnect(_board_addresses[dead_board_id]);
    _board_addresses.erase(dead_board_id);
    _board_proxy_pub->send(generateMessageFromString("b"), zmq::send_flags::sndmore);
    _board_proxy_pub->send(generateMessageFromString(std::to_string(dead_board_id) + "!"), zmq::send_flags::none);
}
/**
 * @brief function to handle a message containing the information that a board is dead
 *
 * Disconnects from board if subbed to it.
 * Sends an alive message if it receives its own id as id of the dead board.
 * Sends an alive message if the dead boards id is smaller than it's own to make
 * it possible for its predecessors to sub to it.
 *
 * @param msg_str The incoming message in string format.
 * @param dead_character_pos position of the character marking the board is dead in the message string.
 */
void MatrixBoard::handleDeadBoardMessage(std::string msg_str, std::size_t dead_character_pos) {
    msg_str.erase(dead_character_pos);
    std::cout << "dead:" << msg_str << std::endl;
    if (_board_addresses.find(std::stoi(msg_str)) != _board_addresses.end()) {
        std::cout << _board_addresses[std::stoi(msg_str)] << std::endl;
        _sub_socket->disconnect(_board_addresses[std::stoi(msg_str)]);
        std::cout << "erase:" << msg_str << std::endl;
        _board_addresses.erase(std::stoi(msg_str));
        std::cout << "dead board:" << msg_str << std::endl;
    } else if (msg_str == _id || std::stoi(msg_str) < std::stoi(_id)) {
        std::cout << "I'm alive:" << _id << ":send:" << _id + "|" + _MY_ADDRESS << " to:"
                  << "b" << std::endl;
        _board_proxy_pub->send(generateMessageFromString("b"), zmq::send_flags::sndmore);
        _board_proxy_pub->send(generateMessageFromString(_id + "|" + _MY_ADDRESS), zmq::send_flags::none);
    }
}
/**
 * @brief function to handle a message containing the information there's a new board online
 *
 *  Subscribes to the new board if necessary
 *  If the id of the board is smaller than own id, send alive message.
 *  This makes it possible for the new board to sub to this board.
 *
 * @param msg_str The incoming message in string format.
 * @param newboard_character_pos position of the character marking the board is new in the message string.
 */
void MatrixBoard::handleNewBoardMessage(std::string msg_str, std::size_t newboard_character_pos) {
    std::size_t pos = msg_str.find("|");
    int id = std::stoi(msg_str.substr(0, newboard_character_pos));
    std::string address = msg_str.substr(pos + 1, msg_str.size());
    if (processNewBoard(id, address)) {
        _sub_socket->setsockopt(ZMQ_SUBSCRIBE, std::to_string((_board_addresses.begin())->first).c_str(), 1);
        _sub_socket->connect((_board_addresses.begin())->second);
        _sub_socket->setsockopt(ZMQ_SUBSCRIBE, std::to_string((--_board_addresses.end())->first).c_str(), 1);
        _sub_socket->connect((--_board_addresses.end())->second);
    }
    if (id < std::stoi(_id)) {
        std::cout << _id << ":send:" << _id + "|" + _MY_ADDRESS << " to:"
                  << "b" << std::endl;
        _board_proxy_pub->send(generateMessageFromString("b"), zmq::send_flags::sndmore);
        _board_proxy_pub->send(generateMessageFromString(_id + "|" + _MY_ADDRESS), zmq::send_flags::none);
    }
}
/**
 * @brief function to handle a message containing the information there's a board online
 *
 * Subscribes to the alive board if necessary
 *
 * @param msg_str The incoming message in string format.
 */
void MatrixBoard::handleAliveBoardMessage(std::string msg_str) {
    std::size_t pos = msg_str.find("|");
    int id = std::stoi(msg_str.substr(0, pos));
    if (id != std::stoi(_id)) {
        std::string address = msg_str.substr(pos + 1, msg_str.size());
        std::cout << "alive board:" << id << " address:" << address << std::endl;
        if (processNewBoard(id, address)) {
            _sub_socket->setsockopt(ZMQ_SUBSCRIBE, std::to_string((_board_addresses.begin())->first).c_str(), 1);
            _sub_socket->connect((_board_addresses.begin())->second);
            _sub_socket->setsockopt(ZMQ_SUBSCRIBE, std::to_string((--_board_addresses.end())->first).c_str(), 1);
            _sub_socket->connect((--_board_addresses.end())->second);
        }

    } else {
        std::cout << "alive board: self" << std::endl;
    }
}
