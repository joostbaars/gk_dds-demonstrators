#ifndef MEASUREMENTS_FILE_H
#define MEASUREMENTS_FILE_H

#include <fstream>
#include <sstream>
#include <string>
#include <vector>

/**
 *  @brief MeasurementsFile writes new measurements into the measurements file
 *  The new measurements are just appended to the file.
 *  This could still be optimized to classify the measurements and place them together
 */
class MeasurementsFile {
  public:
    MeasurementsFile(const std::string fileName);
    ~MeasurementsFile() = default;

    void createFile(std::vector<std::string> column_names);

    void write(std::vector<std::string> measurements);
    std::string vectorToCommaSeperatedString(std::vector<std::string> vector);

  private:
    std::string _fileName;
};


#endif // MEASUREMENTS_FILE_H