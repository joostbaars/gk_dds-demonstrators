#include "proxy.hpp"

int main(int argc, char const *argv[]) {
    Proxy proxy("", "5551", "5550");
    try {
        proxy.start();
    } catch (const std::exception &e) {
        std::cerr << "Error catched in main: " << e.what();
        return -1;
    }

    return 0;
}
