#ifndef PERFORMANCE_MEASUREMENTS_H
#define PERFORMANCE_MEASUREMENTS_H

#include <chrono>
#include <ctime>
#include <iomanip> // put_time
#include <sstream> // stringstream
#include <string>
/**
 * @brief PerformanceMeasurements contains functions for a timer
 *
 */
class PerformanceMeasurements {
  public:
    PerformanceMeasurements() = default;
    ~PerformanceMeasurements() = default;

    void start();
    void stop();

    int getTime() const;
    std::string getStartMili();

  private:
    std::chrono::time_point<std::chrono::high_resolution_clock> _startingTime;
    std::chrono::time_point<std::chrono::high_resolution_clock> _stoppedTime;
};


#endif // PERFORMANCE_MEASUREMENTS_H