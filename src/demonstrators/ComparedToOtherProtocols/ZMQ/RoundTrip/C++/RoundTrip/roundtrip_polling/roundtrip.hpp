#ifndef ROUNDTRIP_H
#define ROUNDTRIP_H

#include "performance/performance_measurements.hpp"

#include <iostream>
#include <zmq.hpp>

/**
 * @brief RoundTrip contains the implementation of the roundtrip
 *
 */
class RoundTrip {
  public:
    RoundTrip(const unsigned int id, const unsigned int totalDevices, const unsigned int roundTrips, const std::string pubAddress, const std::string subAddress);
    ~RoundTrip();

    int getTime() const { return _timer.getTime<std::chrono::microseconds>(); };

    zmq::message_t generateMessageFromString(std::string message);

    void run();

  private:
    const unsigned int _id;
    const unsigned int _totalDevices;
    unsigned int _totalRoundtrips;

    bool _running;
    unsigned int _roundTrips;

    const std::string _subAddress;
    const std::string _pubAddress;
    std::string _writeTopic;
    std::string _readTopic;

    zmq::context_t _context;
    zmq::socket_t _subSocket;
    zmq::socket_t _pubSocket;
    
    PerformanceMeasurements _timer;

    void initialise();
};

#endif // ROUNDTRIP_H