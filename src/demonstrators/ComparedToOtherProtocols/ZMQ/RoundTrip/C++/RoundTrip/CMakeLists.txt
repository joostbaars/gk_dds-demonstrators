cmake_minimum_required(VERSION 3.5)

# Set name of the executable
# The project can be roundtrip_read or roundtrip_callback
# roundtrip_read: polls the reading and reads the value when it is received
# roundtrip_callback: executes a callback function when data is received
# Note: This is the only configurable parameter of this file!
set(PROJECT roundtrip_polling)

set(NAME_PROJECT RoundTrip)

# Define the project as a CPP project
project(${NAME_PROJECT})

#[[
    Add the files to the project
]]
# Relative include path
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/
                     ${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT}/
                     ${CMAKE_CURRENT_SOURCE_DIR}/../../../../../CommonCode/C++/)

set(SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT}/roundtrip.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/../../../../../CommonCode/C++/performance/performance_measurements.cpp)

# Set the main
set(MAIN ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp)

# Add the sources and main to the executable
add_executable(${NAME_PROJECT} ${MAIN} ${SOURCES})

set_property(TARGET ${NAME_PROJECT} PROPERTY CXX_STANDARD 17)

# Add warnings
target_compile_options(${NAME_PROJECT} PRIVATE -pedantic -Wall -Wcast-align -Wcast-qual -Wdisabled-optimization 
                                               -Winit-self -Wmissing-declarations -Wmissing-include-dirs -Wredundant-decls 
                                               -Wshadow -Wsign-conversion -Wundef -Werror
                                               -Wempty-body -Wignored-qualifiers -Wmissing-field-initializers 
                                               -Wsign-compare -Wtype-limits  -Wuninitialized )

#[[
    Installed libraries for this project
]]
### Threading library
find_package(Threads REQUIRED)

# Add the threading library to the project
set(DEPENDENCIES ${CMAKE_THREAD_LIBS_INIT})

### ZMQ Library (cppzmq)
find_package(cppzmq REQUIRED)

# Add the ZMQ library to the project
target_link_libraries(${NAME_PROJECT} cppzmq)

#[[
    Linking the libraries
]]
# Link libraries
target_link_libraries(${NAME_PROJECT} ${DEPENDENCIES})