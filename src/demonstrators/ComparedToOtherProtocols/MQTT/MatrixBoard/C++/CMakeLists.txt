cmake_minimum_required(VERSION 3.10)

find_package(PahoMqttCpp REQUIRED)

add_executable(mqttmatrixboard main.cpp matrixboard.cpp car_sensor.cpp matrixboard_display.cpp measurements_file.cpp performance_measurements.cpp)

target_link_libraries(mqttmatrixboard paho-mqttpp3 paho-mqtt3as)

add_executable(measurementsfiles main_measurements_file.cpp measurements_file.cpp) 