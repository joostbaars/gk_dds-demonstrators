#ifndef MATRIXBOARD_H
#define MATRIXBOARD_H

#include <atomic>
#include <string>
//#include <iostream>
#include <chrono>
#include <map>
#include <memory>
#include <mutex>
#include <thread>

#include "car_sensor.hpp"
#include "matrixboard_display.hpp"
#include "mqtt/client.h"
#include "performance_measurements.hpp"
#include "measurements_file.hpp"

/**
 * @brief MatrixBoardSim contains the functions for the matrixboard simulation
 *
 */
class MatrixBoard {
  public:
    // Constructor & Destructor
    MatrixBoard(std::string id, std::string serverAddress);
    ~MatrixBoard();
    void run();
    void processNewBoard(std::string new_board);
    void removeBoardFromSubscriptions();
    void subscribeToBoards();

  private:
    const std::string _SERVER_ADDRESS;
    std::string _id;
    unsigned int _cars_self;
    std::map<int, int> _nextboards;
    bool _change = false;
    int _erase = -1;
    mqtt::async_client _client;
    bool _notify_liveliness = false;
    std::mutex _boards_mutex;

  protected:
    matrixboard::MatrixBoardDisplay _display;
    matrixboard::CarSensor _sensor;

    unsigned int calculateSpeed(const unsigned int traffic);
};

#endif // !MATRIXBOARD_H