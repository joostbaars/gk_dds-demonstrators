#ifndef PERFORMANCE_MEASUREMENTS_HPP
#define PERFORMANCE_MEASUREMENTS_HPP

#include <chrono>
#include <ctime>
#include <sstream>
#include <string>

class PerformanceMeasurements {
    public:
        PerformanceMeasurements() = default;
        ~PerformanceMeasurements() = default;

        void start();
        void stop();

        std::string getStartMili();

    private:
        std::chrono::time_point<std::chrono::high_resolution_clock> _startingTime;
        std::chrono::time_point<std::chrono::high_resolution_clock> _stoppedTime;

};

#endif