#include <chrono>
#include <string>
#include <thread>

#include "roundtrip.hpp"

/**
 * @brief Construct a new Round Trip:: Round Trip object
 *
 * @param id the ID of the device
 * @param totalDevices the total number of devices in the roundtrip
 * @param roundTrips the number of roundtrips that will be executed
 * @param server_address the address of the MQTT server
 * @param qos the MQTT QoS level that should be used
 */
RoundTrip::RoundTrip(const unsigned int id, const unsigned int totalDevices, const unsigned int roundTrips, const std::string server_address, const int qos)
    : _id{id}, _totalDevices{totalDevices}, _qos{qos}, _totalRoundtrips{roundTrips}, _message{false}, _running{false}, _roundTrips{0},
    _client{server_address, std::to_string(id)}, 
    TOPICS{ "roundtrip" + std::to_string(id), (id == totalDevices ? "roundtrip1" : "roundtrip" + std::to_string(id+1)) }
    {
    initialise();
}

/**
 * @brief initialise initialises the round trip and the correct settings for DDS
 *
 */
void RoundTrip::initialise() {
    mqtt::connect_options connOpts;
    connOpts.set_keep_alive_interval(10);
    connOpts.set_mqtt_version(MQTTVERSION_5);

    _client.connect(connOpts);

    _client.subscribe(TOPICS[0], _qos);
}

/**
 * @brief run runs the roundtrip
 *
 * @note this function could wait infinitely!
 */
void RoundTrip::run() {
    _running = true;
    mqtt::const_message_ptr msg;
    _timer.start();
    // Initiate the roundtrip
    if (_id == 1) {
       _client.publish(mqtt::message(TOPICS[1], std::to_string(_id), _qos, false));
    }
    while (_running) {    
        if(_client.try_consume_message(&msg)) {
            _client.publish(mqtt::message(TOPICS[1], std::to_string(_id), _qos, false));
            if (_totalRoundtrips == ++_roundTrips) {
                _running = false;
            }
        }   
    }
    _timer.stop();
}