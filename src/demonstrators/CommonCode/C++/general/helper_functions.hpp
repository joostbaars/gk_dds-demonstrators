#ifndef HELPERFUNCTIONS_H
#define HELPERFUNCTIONS_H

#include <sstream>
#include <string>

/**
 * @brief Contains generic functions
 */
class HelperFunctions {
  public:
    template <typename... Args>
    static std::string argToString(const Args &... args);
    
    static int getRandomTime(const int minimum, const int maximum);
  private:
};

/** @brief argToString converts arguments to a string
 *
 * @param args constant arguments list which will be converted to a string
 * @return a string with all the arguments
 */
template <typename... Args>
std::string HelperFunctions::argToString(const Args &... args) {
    std::ostringstream ss;
    ((ss << args), ...);
    return ss.str();
}

#endif // HELPERFUNCTIONS_H