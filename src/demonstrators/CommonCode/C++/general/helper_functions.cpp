#include "helper_functions.hpp"

#include <random>

/**
 * @brief returns a random time between minimum and maximum
 * 
 * @param minimum the minimum value that may be returned
 * @param maximum the maximum value that may be returned
 * @return int the random returned value
 */
int HelperFunctions::getRandomTime(const int minimum, const int maximum) {
    std::random_device seed;        // Will be used to obtain a seed for the random number engine
    std::mt19937 generator(seed()); // Standard mersenne_twister_engine seeded with seed()
    std::uniform_int_distribution<> distribution(static_cast<int>(minimum), static_cast<int>(maximum));
    return distribution(generator);
}