#include "logger.hpp"

#include <cmath>
#include <sys/time.h>

/**
 * @brief Creates a time stamp and returns it as a string
 *
 * @return std::string the time stamp of the current time
 */
std::string Logger::currentTimeStamp() {
    char datetime[25];
    time_t now = time(NULL);

    struct tm localtime;
    struct tm time = *localtime_r(&now, &localtime);

    strftime(datetime, 1000, "%Y-%m-%d %X", &time);
    int milliseconds = getMilliseconds();

    char datetime_milliseconds[30];
    sprintf(datetime_milliseconds, "%s.%03d", datetime, milliseconds);
    return datetime_milliseconds;
}

/**
 * @brief Creates a time stamp and returns it as a string
 *
 * @return std::string the time stamp of the current time
 */
std::string Logger::currentTimeStampAccurate() {
    char datetime[23];
    time_t now = time(NULL);

    struct tm localtime;
    struct tm time = *localtime_r(&now, &localtime);

    strftime(datetime, 1000, "%Y-%m-%d %X", &time);
    int microseconds = getMicroseconds();

    char datetime_microseconds[30];
    sprintf(datetime_microseconds, "%s.%6d", datetime, microseconds);
    return datetime_microseconds;
}

/**
 * @brief returns millisecond accuracy for a timestamp
 *
 * @return int the current milliseconds of the clock
 */
int Logger::getMilliseconds() {
    int milliseconds;
    struct timeval tv;
    gettimeofday(&tv, NULL);

    milliseconds = lrint(tv.tv_usec / 1000.0);
    if (milliseconds >= 1000) {
        milliseconds -= 1000;
        tv.tv_sec++;
    }
    return milliseconds;
}

/**
 * @brief returns microseconds + milliseconds accuracy for accurate logging purposes
 *
 * @return int the current microseconds + milliseconds of the clock
 */
int Logger::getMicroseconds() {
    constexpr int second = 1000000;
    int microseconds;
    struct timeval tv;
    gettimeofday(&tv, NULL);

    microseconds = lrint(tv.tv_usec);
    if (microseconds >= second) {
        microseconds -= second;
    }
    return microseconds;
}