#ifndef RESOURCE_USAGE_HPP
#define RESOURCE_USAGE_HPP

#include <string>
#include <vector>

#include "general/timer.hpp"

/**
 * @brief ResourceUsage contains the functions for measuring the resource usage within Linux
 *
 */
class ResourceUsage {
  public:
    explicit ResourceUsage(const unsigned int applicationPID = 0);
    ~ResourceUsage() = default;

    using kilobytes = int;
    using percentage = double;
    static kilobytes getVirtualMemoryUsage(const unsigned int applicationPID = 0);
    static kilobytes getPhysicalMemoryUsage(const unsigned int applicationPID = 0);
    percentage getCPUusage();

  private:
    int _cpuUsage1, _procTimes1;
    static int _numProcessors;
    bool _initialized;
    const std::string _applicationPID;
    Timer<int64_t, std::milli> _timer;

    static kilobytes getVirtualMemoryUsageStr(const std::string &applicationPID = "self");
    static kilobytes getPhysicalMemoryUsageStr(const std::string &applicationPID = "self");

    static int parseLine(const std::string &line);
    static void initializeCPUusage();
    static int getParameter(const std::string &location, const std::string &parameterName);
    static int countParameter(const std::string &location, const std::string &parameterName);
    static std::string deleteNonNumericAndSpace(const std::string &input);
    static inline bool noDigitOrSpace(char input);
    static std::vector<int> stringToVector(const std::string &input);
    static int getTotalCPUusage();
    int getProcessCPUusage() const;
};

inline bool ResourceUsage::noDigitOrSpace(char input) {
    if ((input >= '0' && input <= '9') || input == ' ') {
        return false;
    }
    return true;
}

#endif // RESOURCE_USAGE_HPP