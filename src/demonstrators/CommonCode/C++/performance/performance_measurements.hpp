#ifndef PERFORMANCE_MEASUREMENTS_H
#define PERFORMANCE_MEASUREMENTS_H

#include <chrono>

/**
 * @brief PerformanceMeasurements contains functions for a timer
 *
 */
class PerformanceMeasurements {
  public:
    PerformanceMeasurements() = default;
    ~PerformanceMeasurements() = default;

    void start();
    void stop();

    template <typename ReturnType = std::chrono::milliseconds>
    int getTime() const;

  private:
    std::chrono::time_point<std::chrono::high_resolution_clock> _startingTime;
    std::chrono::time_point<std::chrono::high_resolution_clock> _stoppedTime;
};

/**
 * @brief getTime returns the time betwween the start and stop function
 *
 * @tparam ReturnType a template parameter defining the time format that is returned (by default
 * std::chrono::milliseconds)
 * @return int the time between the start and stop function
 */
template <typename ReturnType>
int PerformanceMeasurements::getTime() const {
    return std::chrono::duration_cast<ReturnType>(_stoppedTime - _startingTime).count();
}

#endif // PERFORMANCE_MEASUREMENTS_H