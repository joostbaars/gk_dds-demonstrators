#!/bin/bash

cmake --build $PWD/build/

echo "Deleting old log file."
rm $PWD/roundtrip.log

echo "Running Roundtrip Demonstrator...press ^C to exit"
./build/Hub_roundtrip topic_2 topic_3 &
./build/Hub_roundtrip topic_3 topic_4 &
./build/Hub_roundtrip topic_4 topic_1 &
./build/Master_roundtrip topic_1 topic_2 > roundtrip.log
