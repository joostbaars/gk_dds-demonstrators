#include "roundtrip.hpp"

#include "dds/ddsi/ddsi_xqos.h"
#include "qos_manager/qos_manager.hpp"

#include <iostream>
#include <thread>

/**
 * @brief Construct a new Round Trip:: Round Trip object
 *
 * @param id the ID of the device
 * @param totalDevices the total number of devices in the roundtrip
 * @param roundTrips the number of roundtrips that will be executed
 */
RoundTrip::RoundTrip(const unsigned int id, const unsigned int totalDevices, const unsigned int roundTrips)
    : _id{id}, _totalDevices{totalDevices}, _totalRoundtrips{roundTrips}, _running{false}, _roundTrips{0} {}

/**
 * @brief Destroys the round trip object and deallocates allocated memory
 *
 */
RoundTrip::~RoundTrip() {
    dds_delete(_participant);
    PerformanceData_Msg_free(_messageMemory[0], DDS_FREE_ALL);
}

/**
 * @brief run runs the roundtrip
 *
 * @note this function could wait infinitely!
 */
void RoundTrip::run() {
    dds_return_t readCheck;
    dds_sample_info_t infos[1];

    _running = true;
    _timer.start();
    // Initiate the roundtrip
    if (_id == 1) {
        dds_write(_writer, &_msg);
    }
    while (_running) {
        readCheck = dds_take(_reader, _messageMemory, infos, 1, 1);
        if ((readCheck > 0) && (infos[0].valid_data)) {
            dds_write(_writer, &_msg);
            if (_totalRoundtrips == ++_roundTrips) {
                _running = false;
            }
        }
    }
    _timer.stop();
    _running = false;
}

/**
 * @brief initialises the round trip
 *
 * @param qos a pointer to the class containing the QoS objects as well as the listener object
 */
void RoundTrip::initialise(QoSManager *qos) {
    _participant = dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL);
    std::string readTopic = "roundtrip" + std::to_string(_id);
    std::string writeTopic = "roundtrip" + std::to_string(_id + 1);
    if (_id == _totalDevices) {
        writeTopic = "roundtrip1";
    }

    _topicWrite = dds_create_topic(_participant, &PerformanceData_Msg_desc, writeTopic.c_str(), NULL, NULL);
    _topicRead = dds_create_topic(_participant, &PerformanceData_Msg_desc, readTopic.c_str(), NULL, NULL);
    _writer = dds_create_writer(_participant, _topicWrite, qos->getWriter(), NULL);
    _reader = dds_create_reader(_participant, _topicRead, qos->getReader(), NULL);

    _msg.key = static_cast<int32_t>(_id);

    _messageMemory[0] = PerformanceData_Msg__alloc();

    // Sleep for initialisation
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
}