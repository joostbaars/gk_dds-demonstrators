#ifndef FLOODFILL_H
#define FLOODFILL_H

#include "performance/performance_measurements.hpp"

#include "PerformanceData.h"
#include "dds/dds.h"
#include "qos_manager/qos_manager.hpp"

#include <chrono>

/**
 * @brief Flood contains the implementation of the floodfill
 *
 */
class Flood {
  public:
    Flood(const unsigned int id, const unsigned int totalDevices, const unsigned int floodMessages);
    ~Flood();

    int getTime() const { return _timer.getTime<std::chrono::microseconds>(); };

    void initialise(QoSManager *qos);
    void run();

  private:
    const unsigned int _id;
    const unsigned int _totalDevices;
    unsigned int _totalFloodMsg;

    bool _running;
    unsigned int _floodSend;
    unsigned int _floodReceived;
    PerformanceData_Msg _msg;
    dds_entity_t _writer, _reader;
    dds_entity_t _participant;
    dds_entity_t _topicWrite, _topicRead;
    void *_messageMemory[1];

    PerformanceMeasurements _timer;

    void runMaster();
    void runSlave();
};

#endif // FLOODFILL_H