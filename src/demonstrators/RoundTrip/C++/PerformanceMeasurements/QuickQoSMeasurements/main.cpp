#include <iostream>

#include "performance_tests/performance_tests.hpp"

void errorMessage();

/**
 * @brief main contains the main code for the roundtrip project
 *
 * @param argc number of arguments
 * @param argv the arguments of the function
 * @return int endstate of the application
 */
int main(int argc, char const *argv[]) {
    if (argc != 4 && argc != 5) {
        errorMessage();
        return -1;
    }
    unsigned int id;
    unsigned int totalDevices;
    unsigned int totalRoundtrips;
    std::string filename = "measurements.csv";
    try {
        id = std::stoul(argv[1]);
        totalDevices = std::stoul(argv[2]);
        totalRoundtrips = std::stoul(argv[3]);
        if (argc == 5) {
            filename = argv[4];
        }
        if (id == 0 || id > totalDevices || totalDevices == 0) {
            errorMessage();
            return -1;
        }
    } catch (const std::exception &e) {
        errorMessage();
        return -1;
    }
    PerformanceTests tests(id, totalDevices, totalRoundtrips, filename);
    tests.start();
    std::cout << "Tests executed";
    return 0;
}

/**
 * @brief errorMessage contains the fault message for wrong parameters
 *
 */
void errorMessage() {
    std::cerr
        << "Wrong input for this application!\n\n"
        << "Format: ./<Application name> <Device ID> <Total devices> <Total msg / round trips> <Filename>\n\n"
        << "<Application name>: The application you want to run, this can be \"performance_measurements_flood\" or "
           "\"performance_measurements_roundrip\"\n"
        << "<device ID>: The ID of the device, must be unique, > 0 and <= total devices\n"
        << "<Total devices>: The amount of devices in the loop (the amount of applications running)\n"
        << "<Total msg / round trips>: For the flood application: Total messages send\n"
        << "For the roundtrip application: Number of round trips to execute\n"
        << "<filename>: This is an OPTIONAL parameter! This is the filename where the measurements are stored to, "
           "default: measurements.csv\n\n"
        << "Note: Only the <device ID> is unique! The other 2 parameters MUST be equal for each application!\n";
}