#include "measurements_file.hpp"

/**
 * @brief MeasurementsFile sets the config filename
 *
 * @param fileName the name of the measurements file
 */
MeasurementsFile::MeasurementsFile(const std::string &fileName) : _fileName{fileName} {}
