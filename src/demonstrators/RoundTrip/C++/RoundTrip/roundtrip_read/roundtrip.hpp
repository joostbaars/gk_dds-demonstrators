#ifndef ROUNDTRIP_H
#define ROUNDTRIP_H

#include "performance/performance_measurements.hpp"

#include "RoundTripData.h"
#include "dds/dds.h"

/**
 * @brief RoundTrip contains the implementation of the roundtrip
 *
 */
class RoundTrip {
  public:
    RoundTrip(const unsigned int id, const unsigned int totalDevices, const unsigned int roundTrips);
    ~RoundTrip();

    int getTime() const { return _timer.getTime<std::chrono::microseconds>(); };

    void run();

  private:
    const unsigned int _id;
    const unsigned int _totalDevices;
    unsigned int _totalRoundtrips;

    bool _running;
    unsigned int _roundTrips;
    RoundTripData_Msg _msg;
    dds_entity_t _writer;
    dds_entity_t _reader;
    dds_entity_t _participant;
    dds_entity_t _topicWrite, _topicRead;
    void *_messageMemory[1];

    PerformanceMeasurements _timer;

    void initialise();
};

#endif // ROUNDTRIP_H