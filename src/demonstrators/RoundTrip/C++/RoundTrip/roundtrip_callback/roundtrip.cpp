#include <chrono>
#include <string>
#include <thread>

#include "roundtrip.hpp"

unsigned int RoundTrip::_roundTrips{0};
unsigned int RoundTrip::_totalRoundtrips{0};
dds_entity_t RoundTrip::_writer;
RoundTripData_Msg RoundTrip::_msg;
PerformanceMeasurements RoundTrip::_timer{};
bool RoundTrip::_running{false};

/**
 * @brief Construct a new Round Trip:: Round Trip object
 *
 * @param id the ID of the device
 * @param totalDevices the total number of devices in the roundtrip
 * @param roundTrips the number of roundtrips that will be executed
 */
RoundTrip::RoundTrip(const unsigned int id, const unsigned int totalDevices, const unsigned int roundTrips)
    : _id{id}, _totalDevices{totalDevices} {
    _totalRoundtrips = roundTrips;
    initialise();
}

/**
 * @brief Destroy the Round Trip:: Round Trip object including allocated memory
 *
 */
RoundTrip::~RoundTrip() {
    dds_delete(_participant);
    dds_delete_listener(_listener);
}

/**
 * @brief initialise initialises the round trip and the correct settings for DDS
 *
 */
void RoundTrip::initialise() {
    _participant = dds_create_participant(DDS_DOMAIN_DEFAULT, NULL, NULL);
    std::string readTopic = "roundtrip" + std::to_string(_id);
    std::string writeTopic = "roundtrip" + std::to_string(_id + 1);
    if (_id == _totalDevices) {
        writeTopic = "roundtrip1";
    }
    _listener = dds_create_listener(NULL);
    dds_lset_data_available(_listener, callBack);
    _topicWrite = dds_create_topic(_participant, &RoundTripData_Msg_desc, writeTopic.c_str(), NULL, NULL);
    _topicRead = dds_create_topic(_participant, &RoundTripData_Msg_desc, readTopic.c_str(), NULL, NULL);
    _writer = dds_create_writer(_participant, _topicWrite, NULL, NULL);
    _reader = dds_create_reader(_participant, _topicRead, NULL, _listener);

    _msg.key = static_cast<int32_t>(_id);

    // Sleep for initialisation
    std::this_thread::sleep_for(std::chrono::milliseconds(50));
}

/**
 * @brief initiateRoundtrip initiates the roundtrip
 *
 */
void RoundTrip::initiateRoundtrip() {
    // Initiate the roundtrip
    if (_id == 1) {
        dds_write(_writer, &_msg);
        _timer.start();
    }
    _running = true;
}

/**
 * @brief run runs a loop until the roundtrip is done
 *
 */
void RoundTrip::run() {
    initiateRoundtrip();

    while (_running) {
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

/**
 * @brief callBack contains the callback function for when data is received
 *
 * @param reader the reader that initiated this callback function
 * @param arg arguments
 *
 * @note this callback function writes to the next node in the roundtrip
 */
void RoundTrip::callBack(dds_entity_t reader, void *arg) {
    if (_totalRoundtrips != _roundTrips) {
        dds_write(_writer, &_msg);
        _roundTrips++;
    } else {
        _timer.stop();
        _running = false;
    }
}