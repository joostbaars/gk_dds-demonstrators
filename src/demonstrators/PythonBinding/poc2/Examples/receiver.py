from sys import path
path.append("..")
from dds_library import topic, Reader, Listener, Domain, Key
import time
import logging
import ctypes

logging.getLogger().setLevel(logging.INFO)


domain = Domain(1)

@topic(domain)
class Data:
    def __init__(self, **kwargs):
        self.name  = str
        self.id    = Key(int)
        self.flag  = bool
    
    def __repr__(self):
        return f"| name: {self.name}, id: {self.id}, flag: {self.flag}|"


reader = Reader(Data, 10)

while True:
    msg = reader()
    logging.info(msg)
    time.sleep(3)