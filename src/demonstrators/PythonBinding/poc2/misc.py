import ctypes
from caccess import TYPES

BUILTINS = [int, float, bool, str]


class int16:
    ctype = ctypes.c_int16
    default = 0
    op_adr = TYPES.DDS_OP_TYPE_2BY

class int32:
    ctype = ctypes.c_int32
    default = 0
    op_adr = TYPES.DDS_OP_TYPE_4BY

class int64:
    ctype = ctypes.c_int64
    default = 0
    op_adr = TYPES.DDS_OP_TYPE_8BY

class string:
    ctype = ctypes.c_char_p
    default = ""
    op_adr = TYPES.DDS_OP_TYPE_STR

class boolean:
    ctype = ctypes.c_bool
    default = False
    op_adr = TYPES.DDS_OP_TYPE_1BY

class double:
    ctype = ctypes.c_double
    default = 0.0
    op_adr = TYPES.DDS_OP_TYPE_8BY

class floating:
    ctype = ctypes.c_float
    default = 0.0
    op_adr = TYPES.DDS_OP_TYPE_4BY

native_dict = {
    int: int32,
    float: double,
    str: string,
    bool: boolean
}