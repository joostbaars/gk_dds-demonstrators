from typing import Tuple, Any, Type, TypeVar, List, Dict
import ctypes
from misc import BUILTINS, native_dict, int32
from stubtypes import KeyDescriptor, TopicDescriptor, SampleInfo
from caccess import create_listener, create_participant, create_reader, create_topic, create_writer, write, take,\
    DDS_OP_ADR, TYPES, DDS_OP_RTS, DDS_OP_FLAG_KEY, DDS_OP_RTS, DDS_TOPIC_NO_OPTIMIZE


A = TypeVar("A")

class Domain:
    """
    Representing a DDS endpoint.
    """
    def __init__(self, domain_id:int):
        self.domain_id = create_participant(domain_id, ctypes.cast(None, ctypes.c_void_p), ctypes.cast(None, ctypes.c_void_p))
    
    def __int__(self):
        return self.domain_id

    def __str__(self):
        return f"Participant object with id: {self.domain_id.__str__()}"


class Listener:
    def __init__(self):
        self.id = create_listener(None)

    def __str__(self):
        return f"Listener object with id: {self.id.__str__()}"

    def __int__(self):
        return self.id


class Writer:
    """
    Writer class. Domain ID is inferred from the topic.
    """
    def __init__(self, topic):
        """
        :param topic: The topic to write to.
        """
        if hasattr(topic, "domain") and hasattr(topic, "topic_id"):
            self.topic = topic
            self.id = create_writer(int(topic.domain), topic.topic_id, None, None)
        else:
            raise RuntimeError(f"Failed to create a writer for the topic {topic}.\
            It needs to be a class decorated with @topic(d:Domain)")

    def __str__(self):
        return f"Writer object with id: {self.id.__str__()}"

    def __int__(self):
        return self.id

    def __call__(self, d):
        """
        Publishes the provided object. (Neds to have the type that is
        provided to __init__).
        """
        if hasattr(d, "dds"):
            msg = d.dds()
        else:
            raise RuntimeError(f"Attempted to send incompatible type {type(d)}.")
        return write(self.__int__(), ctypes.byref(msg))


class Reader:
    def __init__(self, topic, size:int,  listener:Listener=Listener()):
        """
        :param topic: Class decorated with @topic(d:Domain), topic to
        be read.
        :param size: Size of the buffer. The maximum amount of messages that
        can be stored before reading.
        :param listener: Listener instance.
        """
        domain = topic.domain
        self.topic = topic
        self.listener = listener
        # void pointer array, new items will be stored here after calling __call__(self, *)
        self.buffer = (ctypes.c_void_p * size)()
        self.info_vec = (SampleInfo * size)()
        self.id = create_reader(int(domain), topic.topic_id, None, int(listener))
        self.size = size

    def __len__(self):
        return self.size

    def __call__(self):
        """
        Reads from the buffer, and returns a list of objects, casted to the specified type. 
        """
        n_read = take(ctypes.c_int32(self.id), self.buffer, self.info_vec, ctypes.c_ulong(self.size), ctypes.c_uint32(self.size))
        rval = self.buffer[:n_read]
        return list(map(lambda x: self.topic.from_dds(ctypes.cast(x, ctypes.POINTER(self.topic.ctype))[0]), rval))
            

    def __str__(self):
        return f"Reader object with id: {self.id}"

    def __int__(self):
        return self.id


class Key:
    """
    This class is used as a wrapper to represent keys.
    """
    def __init__(self, t: type):
        self.t = t

def get_ctype_defval_op(t: Type[A]) -> Tuple[ctypes._SimpleCData, A, int]:
    """
    Given a type, it returns a tuple with the ctype equivalent,
    its default value and the associated ops. Types can be either 
    atomic types or classes that are decorated with @topic, or 
    one of the above wrapped in Key().
    """
    if type(t) is Key:
        t = t.t
    if t in BUILTINS:
        t = native_dict[t]
    if hasattr(t, "ctype") and hasattr(t, "default"):
        if hasattr(t, "op_adr"):
            return t.ctype, t.default, t.op_adr
        else:
            return t.ctype, t.default, TYPES.DDS_OP_TYPE_STU
    else:
        raise RuntimeError("Invalid attribute type in topic definition.")

def get_value(t: Type[A], v: A):
    """
    Gets a value of any type, and converts it to its c value.
    * str to byte string pointer.
    * numeric to c numeric.
    * ctypes.Structure to its self.ctype (it needs to be decorated with @topic).
    :param t: The target type (ctype).
    :param v: The Python value of the field.
    :return: The value v in type t.
    """
    if issubclass(t, ctypes._SimpleCData):
        if type(v) is str:
            return t(v.encode())
        else:
            return t(v)
    elif hasattr(v, "dds"):
        return v.dds()


def topic(domain: Domain):
    """
    :param domain: The domain participant on which this class will be used.
    """
    def decorator(cls:type) -> type:
        old_init = cls.__init__
        def new_init(self, **kwargs):
            """
            We replace the __init__ of the decorated class with this function.
            It first executes the original __init__, then reads out all attributes
            (they should be set to types), saves them in self.types, and replaces the
            values of the attributes with their types' default values. It also supports
            keywords arguments (where the keys are the same as the attribute names) to
            initialize them to anything other than default value.
            """
            old_init(self)
            # A list of ctypes for each field. It is used to build up the ctype structure
            # for the topic.
            types: List[(str, type)] = list()
            python_types: List[(str, type)] = list()
            keydescriptors: List[KeyDescriptor] = list()
            ops: List[ctypes.c_uint32] = list()
            for i, v in enumerate(vars(self)):
                attr_type = getattr(self, v)
                iskey = 0
                if type(attr_type) is Key:
                    attr_type = attr_type.t
                    keydescriptors.append(KeyDescriptor(v.encode(), i))
                    iskey = DDS_OP_FLAG_KEY
                ctp, defval, op = get_ctype_defval_op(attr_type)
                ops.append(ctypes.c_uint32(DDS_OP_ADR | op | iskey))
                # Initializing the attributes to default values.
                self.__setattr__(v, defval)
                # Adding the ctype to types.
                types.append((v, ctp))
                python_types.append((v, attr_type))
            self.__setattr__("types", types)
            self.__setattr__("keydescriptors", keydescriptors)
            self.__setattr__("ops", ops)
            cls.python_types = python_types
            # Initialize the values (optional).
            for k, v in kwargs.items():
                self.__setattr__(k, v)

        cls.__init__ = new_init
        # Instantiate a default object, and add it as a class attribute. It is needed to create the 
        # ctype structure.
        cls.default = cls()
        # Creating the ctypes.Structure type that represents this topic.
        cls.ctype = type(f"c_{cls.__name__}", (ctypes.Structure,), {"_fields_": cls.default.types})
        def to_dds(self) -> ctypes.Structure:
            """
            This function converts the object into a ctypes.Structure object,
            that can be sent by DDS. Instantiates self.ctype.
            """
            # A list containing the values of all fields, in order.
            values = list()
            for n, t in self.types:
                values.append(get_value(t, getattr(self, n)))
            return type(self).ctype(*values)

        cls.dds = to_dds

        def from_dds(struct):
            """
            Creates an intance of itself from the provided ctypes data.
            """
            def unmarshall(n, o):
                """
                :param n: name of attribute.
                :param o: the ctypes object.
                """
                t = type(o)
                if t is bytes:
                    return o.decode()
                if t is ctypes.c_char_p:
                    return o.value.decode()
                elif issubclass(t, ctypes.Structure) and hasattr(o, "python_types"):
                    return dict(cls.python_types)[n].from_dds(o)
                elif hasattr(o, "value"):
                    # Todo: assumes numeric ctype
                    return o.value
                elif t in BUILTINS:
                    return o
                else:
                    raise RuntimeError(f"Unexpected type {t}")

            args : Dict[(str, Any)] = dict()
            for n, _ in cls.python_types:
                args[n] = unmarshall(n, getattr(struct, n))
            
            return cls(**args)

        cls.from_dds = from_dds
        
        # Generating ops
        ops: List[ctypes.c_uint32] = list()
        for op, (n, _) in zip(cls.default.ops, cls.ctype._fields_):
            ops.extend([op, ctypes.c_uint32(getattr(cls.ctype, n).offset)])
        ops.append(ctypes.c_uint32(DDS_OP_RTS))


        # Creating the topic descriptor.
        n_keys = len(cls.default.keydescriptors)
        cls.topic_descriptor = TopicDescriptor(
            ctypes.sizeof(cls.ctype),
            ctypes.sizeof(ctypes.c_char_p),             # Still unsure about this. It always seems to work tho.
            DDS_TOPIC_NO_OPTIMIZE,
            n_keys,
            "N/A".encode(),
            ctypes.cast((KeyDescriptor*n_keys)(*cls.default.keydescriptors), ctypes.POINTER(KeyDescriptor)),
            len(cls.default.ops) + 1,
            (ctypes.c_uint32 * len(ops))(*ops),
            "N/A".encode()                              # This is supposed to be the generated xml.
        )

        # Creating the DDS topic, and assigning topic ID.
        cls.domain = domain
        cls.topic_id = create_topic(ctypes.c_int32(domain), ctypes.byref(cls.topic_descriptor), cls.__name__.encode(), None, None)
        return cls
    return decorator
