"""
This file manages the binding between Python and C.
"""

import ctypes
from ctypes import cdll, CDLL, Structure
from os.path import join

# Path to the cyclone DDS dll.
LIBPATH = "/usr/local/lib"

cyclone = cdll.LoadLibrary(join(LIBPATH, "libddsc.so"))

# The DDS library functions that are supported at the moment.
create_participant = cyclone.dds_create_participant
create_topic = cyclone.dds_create_topic
create_writer = cyclone.dds_create_writer
write = cyclone.dds_write
create_listener = cyclone.dds_create_listener
create_reader = cyclone.dds_create_reader
take = cyclone.dds_take
delete = cyclone.dds_delete

# OPS

DDS_OP_ADR = 0x01 << 24
DDS_OP_RTS = 0x00 << 24
DDS_OP_FLAG_KEY = 0x01

class VALUES:
    DDS_OP_VAL_1BY = 0x01
    DDS_OP_VAL_2BY = 0x02
    DDS_OP_VAL_4BY = 0x03
    DDS_OP_VAL_8BY = 0x04
    DDS_OP_VAL_STR = 0x05
    DDS_OP_VAL_BST = 0x06
    DDS_OP_VAL_SEQ = 0x07
    DDS_OP_VAL_ARR = 0x08
    DDS_OP_VAL_UNI = 0x09
    DDS_OP_VAL_STU = 0x0a

class TYPES:
    DDS_OP_TYPE_1BY = VALUES.DDS_OP_VAL_1BY << 16
    DDS_OP_TYPE_2BY = VALUES.DDS_OP_VAL_2BY << 16
    DDS_OP_TYPE_4BY = VALUES.DDS_OP_VAL_4BY << 16
    DDS_OP_TYPE_8BY = VALUES.DDS_OP_VAL_8BY << 16
    DDS_OP_TYPE_STR = VALUES.DDS_OP_VAL_STR << 16
    DDS_OP_TYPE_BST = VALUES.DDS_OP_VAL_BST << 16
    DDS_OP_TYPE_SEQ = VALUES.DDS_OP_VAL_SEQ << 16
    DDS_OP_TYPE_ARR = VALUES.DDS_OP_VAL_ARR << 16
    DDS_OP_TYPE_UNI = VALUES.DDS_OP_VAL_UNI << 16
    DDS_OP_TYPE_STU = VALUES.DDS_OP_VAL_STU << 16

class SUBTYPES:
    DDS_OP_SUBTYPE_1BY = VALUES.DDS_OP_VAL_1BY << 8
    DDS_OP_SUBTYPE_2BY = VALUES.DDS_OP_VAL_2BY << 8
    DDS_OP_SUBTYPE_4BY = VALUES.DDS_OP_VAL_4BY << 8
    DDS_OP_SUBTYPE_8BY = VALUES.DDS_OP_VAL_8BY << 8
    DDS_OP_SUBTYPE_STR = VALUES.DDS_OP_VAL_STR << 8
    DDS_OP_SUBTYPE_BST = VALUES.DDS_OP_VAL_BST << 8
    DDS_OP_SUBTYPE_SEQ = VALUES.DDS_OP_VAL_SEQ << 8
    DDS_OP_SUBTYPE_ARR = VALUES.DDS_OP_VAL_ARR << 8
    DDS_OP_SUBTYPE_UNI = VALUES.DDS_OP_VAL_UNI << 8
    DDS_OP_SUBTYPE_STU = VALUES.DDS_OP_VAL_STU << 8

# Flags

DDS_TOPIC_NO_OPTIMIZE = 0x0001
DDS_TOPIC_FIXED_KEY = 0x0002
DDS_TOPIC_CONTAINS_UNION = 0x0004