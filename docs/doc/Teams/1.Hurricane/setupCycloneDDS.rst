.. _setupCycloneDDS:

Setup Guide for CycloneDDS 
--------------------------

Geting Started
""""""""""""""

The following are required before building Cyclone DDS:

* C Compiler (i.e. GCC).
* GIT. 
* CMAKE (3.7 or later).
* OpenSSL (1.1 or later)
* Java JDK (8 or later).
* Apache Maven (3.5 or later). 

CycloneDDS Installation
"""""""""""""""""""""""

To download CycloneDDS on Linux/Raspberry Pi:: 

    $ git clone https://github.com/eclipse-cyclonedds/cyclonedds.git 
    $ cd cyclonedds
    $ mkdir build

To build and install CycloneDDS on Linux/Raspberry Pi::

    $ cd build
    $ cmake -DCMAKE_INSTALL_PREFIX=<install-location> ..
    $ cmake --build .
    $ cmake --build . --target install

.. note::

    The two ``cmake --build`` commands might require being executed with ``sudo`` depending on the ``<install-location>``. 

.. warning::

    Apache Maven requires internet access to update the list of artifacts. 
    This will be required when executing ``cmake --build .``. 
    Since the Raspberry Pi is not allowed to connect to the Sogeti network this was achieved using a mobile hotspot.  

Testing CycloneDDS Installation
"""""""""""""""""""""""""""""""

In order to test the CycloneDDS installation on that particular device we are going to run the Hello World example that is provided. Open two terminals and move to the ``~/cyclonedds/build/bin/`` directory and in each terminal run::
    
    $ ./HelloworldSubscriber 
    $ ./HelloworldPublisher 

Notes
"""""

.. note:: 

   In order to customize the configuration of Cyclone DDS create a ``.xml`` file and point to it (in Linux) by::

    $ export CYCLONEDDS_URI=file://$PWD/cyclonedds.xml
   
   For example the following configuration file was utilized to choose the ``wifi0`` network interface over all others. 

   .. code-block:: Console

      <?xml version="1.0" encoding="UTF-8" ?>
      <CycloneDDS xmlns="https://cdds.io/config" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://cdds.io/config https://raw.githubusercontent.com/eclipse-cyclonedds/cyclonedds/master/etc/cyclonedds.xsd">
        <Domain id="any">
            <General>
                <NetworkInterfaceAddress>wifi0</NetworkInterfaceAddress>
            </General>
        </Domain>
    </CycloneDDS>


Tips
""""
.. tip:: 

    Windows installation of CycloneDDS proved somehow problematic, instead a Linux subsystem using Ubuntu was utilized from within Windows. 

    .. todo::

        Install CycloneDDS on a Windows machine. 

Links
"""""
* CycloneDDS Repository: https://github.com/eclipse-cyclonedds/cyclonedds
* Introducing Eclipse Cyclone DDS: https://www.youtube.com/watch?v=-qVHbTTXRKs&t=209s
* Eclipse Cyclone DDS: Data Sharing in the IoT Age: https://www.youtube.com/watch?v=2q86azIbSvA


