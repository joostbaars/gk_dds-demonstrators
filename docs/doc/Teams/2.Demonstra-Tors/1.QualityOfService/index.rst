.. _qos_explanation:

Quality Of Service
------------------

:authors: Joost Baars
:date: Feb 2020

Description
"""""""""""
The Quality of Service (QoS) policies are a set of configurable parameters that 
control the behavior of a DDS system (how and when data is distributed between 
applications). Some of the parameters can alter the resource consumption, fault 
tolerance, or the reliability of the communication.

Each entity (reader, writer, publisher, subscriber, topic, and participant) of DDS 
has associated QoS policies to it. Some policies are only for one entity, others 
can be used on multiple entities. 

More information about the QoS policies as well as a list of existing DDS policies 
can be found in the `Links`_.

QoS findings
""""""""""""
Different tests and measurements are done regarding the Quality of Service policies.
The "General findings" explain some findings that were found while experimenting with 
the Quality of Service policies. There are some quick performance tests executed 
on the round trip and flood demonstrator (roundtrip demonstrator explanation: 
:ref:`demonstrators_roundtrip`). The results ("Quick performance tests") and the 
software of the application ("Quick performance tests software") can be found in 
the list below. The "Matrix board performance measurements" are measurements with 
different QoS policies in the matrix board demonstrator (matrix board explanation: 
:ref:`demonstrators_matrixboard`). These measurements contain the connect/disconnect 
duration and resource usage. See the page below for more information.

.. toctree::
    :titlesonly:
    :maxdepth: 1
    :glob:

    general_qos
    quick_performance_tests
    quick_performance_tests_software
    matrixboard_tests/index

Links
"""""
* General information regarding QoS within DDS: https://community.rti.com/glossary/qos
* List of all QoS policies within DDS: https://community.rti.com/rti-doc/500/ndds.5.0.0/doc/pdf/RTI_CoreLibrariesAndUtilities_QoS_Reference_Guide.pdf
* List of all QoS policies (less information but with hyperlinks): https://community.rti.com/static/documentation/connext-dds/5.2.0/doc/manuals/connext_dds/html_files/RTI_ConnextDDS_CoreLibraries_UsersManual/Content/UsersManual/QosPolicies.htm
* Some examples given by RTI: https://community.rti.com/static/documentation/connext-dds/5.2.0/doc/manuals/connext_dds/html_files/RTI_ConnextDDS_CoreLibraries_UsersManual/Content/UsersManual/ControllingBehavior_withQoS.htm