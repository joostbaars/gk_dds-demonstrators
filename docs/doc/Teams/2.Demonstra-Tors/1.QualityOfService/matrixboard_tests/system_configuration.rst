.. _system_configuration_cpp_mb_perf:

Matrix board performance application system setup
-------------------------------------------------

:authors: Joost Baars
:date: June 2020

Description
"""""""""""
The matrix board application often runs on multiple devices for the best results. 
But those devices must have a synchronized clock to have accurate measurements: 
the clocks of the devices should be synchronized to <1ms difference. The 
precision time protocol (PTP) was chosen for the synchronization between the 
devices. See `Precision time protocol (PTP)`_ for a description of the PTP protocol.

If the application is executed only on one device, this page can be skipped.
This page is not useful if the application is only executed on one device

Install
"""""""
The chosen PTP implementation for Linux based system is the PTP daemon (``ptpd``). 

``ptpd`` can be installed using the following command on Debian based systems 
(for example Ubuntu and Raspbian):

.. code-block:: bash

    sudo apt-get install ptpd

This command downloads the necessary dependencies and installs ``ptpd``. ``ptpd`` 
can also be installed manually by following the following install guide: 
`PTPd install guide`_.

The official GitHub repository of ``ptpd`` can be found here: 
`PTPd GitHub repository`_.

This PTP implementation should also work for FreeBSD, NetBSD, OpenBSD, 
OpenSolaris and Mac OS X.

Setup
"""""
``ptpd`` must run in the background for it to work. There are different 
methods for achieving this. The ``ptpd`` application can be started manually 
every time the performance tests are executed. This application must be started 
on both devices before the performance application is started! This is the 
recommended method when the performance measurements are only executed once or 
twice. This method is explained in `manual execution`_.

When the performance application will be started more often (like when measuring 
different configurations), the method of automatically starting the application 
could be more convenient. This method only works on certain devices and takes more 
work to set-up. This method is explained in `systemctl`_.

manual execution
~~~~~~~~~~~~~~~~
The ``ptpd`` application can be started manually. This can be done by executing the 
command below.

.. code-block:: bash

    sudo /usr/sbin/ptpd -m -i eth0 &

The ``ptpd`` executable is executed with the ``-m`` parameter to 
detect the best active master for time synchronization. The ``-i`` parameter must 
be configured with the interface where PTP must be active on. In the example, 
the first ethernet port was chosen (``eth0``). By checking ``ifconfig``, the 
network names can be found. The preferred network should be chosen. The network 
name for Wi-Fi is often called ``wlan0``.

When it is running, the system configuration is completed and the performance 
application can be started. See :ref:`performance_application_cpp_mb_perf` for 
starting the performance application.

systemctl
~~~~~~~~~
If you have chosen the manual execution, this section can be skipped.

This method uses ``systemctl`` and ``systemd`` as the boot manager. This boot 
manager is not installed on every device and these steps don't always work 
for every device. This method is checked with a Raspberry Pi running 
Raspbian Buster Lite 2020-03-13 and works for this software. On some configurations,
this method does not work, then the manual execution is recommended. 

With ``systemctl``, the initialization of a device can be managed. Services can 
be configured using ``systemctl``.

.. code-block:: bash

    sudo nano /lib/systemd/system/ptpd.service

``nano`` can be replaced with an editor of choice. This ``ptpd.service`` should 
not exist on your system, this command, therefore, creates this file. The 
following must be inserted to the file:

.. code-block:: bash

    [Unit]
    Description=Ptpd accurate time synchronization
    Wants=network-online.target
    After=network.target

    [Service]
    ExecStart=/usr/sbin/ptpd -m -V -i eth0
    Restart=always
    User=root

    [Install]
    WantedBy=multi-user.target

This creates a service for ``systemctl`` called ``ptpd`` (name of the file). 
The application is automatically restarted when it crashes. 

The ``ptpd`` executable is executed with the ``-m`` parameter to 
detect the best active master for time synchronization. The ``-V`` parameter 
places the application in the foreground (necessary for ``systemctl``) and 
enables debugging. The debugging can be seen when executing:

.. code-block:: bash

    systemctl status ptpd
    
The ``-i`` parameter must be configured with the interface where PTP must be 
active on. In the example, the first ethernet port was chosen (``eth0``). 
By checking ``ifconfig``, the network names can be found. The preferred network 
should be chosen. The network name for Wi-Fi is often called ``wlan0``.

The application must first be enabled to be executed on boot. This can be done 
using the command: 

.. code-block:: bash

    sudo systemctl enable ptpd

With this command, the ``ptpd`` service is started on boot.

If you don't restart your device, the service is not started yet. Therefore, the 
first time you must manually start the service. This can be done using the command:

.. code-block:: bash

    sudo systemctl start ptpd

With the status command, the status of the service can be requested. This status 
command is shown below. The third line should note: ``Active: active (running)``.

.. code-block:: bash

    systemctl status ptpd

If PTP is successfully running on all devices, then the configuration is done.

More information regarding ``systemd`` and ``systemctl`` can be found here: 
:ref:`systemctl_basics`.

.. note:: Time zone

    Make sure the time zone is the same for both devices because otherwise the 
    time synchronization won't work correctly.

.. note:: ptpd service

    This service relies on the network and may only start after the network 
    is configured. Therefore, it could also not work on systems with ``systemd`` 
    installed. Then, the application should be started after a different service. 
    The ptpd service can also     be started manually upon boot using 
    ``systemctl restart ptpd``. More information regarding ``systemd`` and 
    ``systemctl`` can be found here: :ref:`systemctl_basics`.

Precision time protocol (PTP)
"""""""""""""""""""""""""""""
PTP is a method to synchronize devices over a local area 
network (LAN). PTP should be capable of synchronizing multiple clocks to less 
than 100 nanoseconds on a network. PTP works best with hardware timestamping but 
a network card capable of hardware timestamping is necessary for this. The 
device that is used for the performance measurements in 
:ref:`matrixboard_performance_measurements_cpp` is the Raspberry Pi. This 
microcomputer does not have hardware timestamping and is therefore limited to 
software timestamping. Synchronization of ~10 microseconds should be doable but 
10-100 microseconds is typical for software timestamping with PTP (according to 
`PTP general information`_).

This data originates from the following source: `PTP general information`_.

NTP
"""
NTP was also researched for time synchronization between multiple local devices.
NTP should also be able to do <1ms synchronizing on local networks. But the setup 
was more difficult because an NTP server had to be created. NTP was also unstable 
on the Raspberry Pi (sometimes it stopped running or stopped finding the correct 
time). This was probably due to that the Raspberry Pi contains ``timesyncd`` by 
default for time synchronization, but ``ntpd`` was necessary to run an NTP server 
(``timesyncd`` is only for NTP clients). ``timesyncd`` and ``ntpd`` cannot run 
together, and that gave issues configuring it. 

Links
"""""
* `Hardware timestamping`_
* `PTP general information`_
* `PTPd GitHub repository`_
* `PTPd install guide`_
* `More information about Systemctl`_

.. _PTP general information: https://endruntechnologies.com/pdf/PTP-1588.pdf
.. _Hardware timestamping: https://license.fsmlabs.com/docs/advanced/advanced_hwstamps.html
.. _PTPd GitHub repository: https://github.com/ptpd/ptpd
.. _PTPd install guide: https://raw.githubusercontent.com/ptpd/ptpd/master/INSTALL 
.. _More information about Systemctl: https://www.linode.com/docs/quick-answers/linux-essentials/introduction-to-systemctl/