.. _requirements_joost:

Requirements of performance measurements for DDS
------------------------------------------------
This page contains the requirements for the performance measurements in DDS.
It is separated into 2 chapters. One of them contains the requirements of 
the client. The other chapter contains the specifications. The specifications 
describe the requirements in a `SMART <https://hr.wayne.edu/leads/phase1/smart-objectives>`_ 
way. These specifications are therefore testable to see if the requirement is 
met. The specifications are, in this project, created by the developer and 
discussed with the client. 

The table below shows all the requirements and specifications. Additionally, it 
shows the specifications that belong to a requirement. This way, the specifications 
linked to a requirement can easily be found.  

.. needtable::
   :tags: documentation, software
   :style: table
   :columns: id;type;title;incoming;outgoing
   :sort: type

Requirements
""""""""""""
The requirements are given by the client and are not SMART defined. 

Documentation
'''''''''''''
The graph below shows the linkage between the requirements and the corresponding 
specifications. These requirements are all related to the documentation.

The requirement of the team pages does not contain any SMART defined specifications. 
The team pages can be used for documenting things quickly and less precise. There are 
no additional specifications bound to this requirement. 

.. needflow::
   :tags: documentation

.. req:: readable documentation
   :ID: Doc_1
   :tags: documentation

   The documentation must be readable for developers who want to learn more about DDS.

.. req:: formal documentation
   :ID: Doc_2
   :tags: documentation

   The formal documentation is the documentation for the delivered artefacts and should 
   be clear and concise. 

.. req:: team pages documentation
   :ID: Doc_3
   :tags: documentation

   The team pages may have less concise language, there are no additional requirements 
   for the team page documentation other than the General requirements.

Software
''''''''
The graph below shows the link between the requirements and the corresponding 
specifications. These requirements are all related to the software that shall be 
developed during the project.

.. needflow::
   :tags: software

.. req:: performance of DDS
   :ID: Perf_1
   :tags: software

   The performance of DDS shall be measured with various configurations.

.. req:: learning
   :ID: Learn_1
   :tags: software

   The delivered artefacts are made for learning/explaining DDS.

.. req:: commercial availability
   :ID: Commer_1
   :tags: software

   The delivered software can be used in commercial products.

.. req:: API's
   :ID: API_1
   :tags: software

   The effect of different API's for DDS must be compared with each other, if possible.

Specifications
""""""""""""""
The specifications are defined with the client. These are derived from the main 
requirements.

Documentation
'''''''''''''
The specifications for the documentation are listed below.

.. spec:: further development documentation
   :ID: DOC_DEV
   :tags: documentation
   :requirement: MUST
   :links: Doc_1

   Every artefact shall be documented in such extent that a developer knows how DDS is 
   used within the artefact. 

   .. note:: This specification can be checked by giving 2 developers the project including 
      the documentation. They should be able to explain how DDS is used within the project. 

.. spec:: documentation instructions
   :ID: DOC_INSTRUCTIONS
   :tags: documentation
   :requirement: MUST
   :links: Doc_1

   Every artefact contains execution instructions that can be followed by developers.

   .. note:: This requirement can be checked by giving 2 developers the project including the 
      documentation. They should be able to successfully execute the instructions.

.. spec:: documentation style
   :ID: DOC_SPH
   :tags: documentation
   :requirement: MUST
   :links: Doc_1

   The documentation shall be written in ReStructured Text for Sphinx.

.. spec:: documentation of Quality of Service policies
   :ID: DOC_QoS
   :tags: documentation
   :requirement: MUST
   :links: Doc_1, PERF_CPU, PERF_MEM, PERF_CONNECT, PERF_DISCONNECT

   The performance differences between Quality of Service policies are documented.

   .. note:: There must be results of performance measurements that show what Quality of Service 
      policies are faster or slower based on the performance measurements (:need:`PERF_CPU`, 
      :need:`PERF_MEM`, :need:`PERF_CONNECT`, :need:`PERF_DISCONNECT`). At least 2 different 
      Quality of Service policies must be compared with each other. 

.. spec:: PlantUML usage
   :ID: DOC_UML
   :tags: documentation
   :requirement: MUST
   :links: Doc_2

   All diagrams within the Sphinx documentation shall be written using PlantUML, if they 
   are supported by the PlantUML language.

   .. note:: This also includes the extended functionalities of PlantUML like Ditaa and DOT.

.. spec:: software documentation of DDS
   :ID: DOC_DDS
   :tags: documentation
   :requirement: MUST
   :links: Doc_2

   The use of DDS within the software shall be documented.

   .. note:: The documentation of the software shall contain a technical explanation of how 
      DDS is used within the application. 

Software
''''''''
The specifications for the software are listed below.

.. spec:: software DDS library
   :ID: PERF_LIB
   :tags: software
   :requirement: MUST
   :links: Perf_1

   The Cyclone DDS library shall be used as the DDS implementation. 
   
   .. note:: Only if something is not implemented in the CycloneDDS library, a different DDS 
      implementation may be used for the tests.

.. spec:: performance CPU
   :ID: PERF_CPU
   :tags: software
   :requirement: SHOULD
   :links: Perf_1

   The CPU usage of different Quality of Service policies shall be measured.

.. spec:: performance memory
   :ID: PERF_MEM
   :tags: software
   :requirement: SHOULD
   :links: Perf_1

   The memory usage of different Quality of Service policies shall be measured.

.. spec:: performance of connecting 
   :ID: PERF_CONNECT
   :tags: software
   :requirement: MUST
   :links: Perf_1

   The time between the DDS registration of a device and the notification of other 
   devices shall be measured.

.. spec:: performance of disconnecting
   :ID: PERF_DISCONNECT
   :tags: software
   :requirement: MUST
   :links: Perf_1

   The time between the DDS disconnection of a device and the notification of other 
   devices shall be measured.

.. spec:: performance difference Quality of Service policies
   :ID: PERF_QoS
   :tags: software
   :requirement: MUST
   :links: Perf_1, PERF_CPU, PERF_MEM, PERF_CONNECT, PERF_DISCONNECT, DOC_QoS

   The performance of Quality of Service policies within DDS shall be compared to 
   each other.

.. spec:: documentation of Quality of Service policies
   :ID: PERF_SCALABLE
   :tags: software
   :requirement: SHOULD
   :links: Perf_1, PERF_CPU, PERF_MEM, PERF_CONNECT, PERF_DISCONNECT

   The difference in performance, CPU/memory footprint, registration time, disconnection 
   time, shall be recorded with 2 devices in the network compared to at least 4 devices 
   in the network.

   .. note:: This requirement tests the scalability of DDS.

.. spec:: software wrapper
   :ID: LEARN_WRAPPER
   :tags: software
   :requirement: SHOULD
   :links: Learn_1

   The functions of Cyclone DDS shall not be used within a wrapper.

.. spec:: commercial license
   :ID: COMMERCIAL_LICENSE
   :tags: software
   :requirement: MUST
   :links: Commer_1

   The delivered software shall be in accordance with the MIT, BSD or Apache licenses, with 
   an exception for the DDS library.

.. spec:: different API's
   :ID: API_TEST
   :tags: software
   :requirement: SHOULD
   :links: API_1

   The communication between 2 different APIs shall be tested (The C/C++ API and the Python API).

.. spec:: performance different API's
   :ID: PERF_API
   :tags: software
   :requirement: SHOULD
   :links: API_1, API_TEST

   The difference in performance, speed of the implementation, shall be measured between 
   different APIs (if this is possible, see requirement :need:`API_TEST`).
