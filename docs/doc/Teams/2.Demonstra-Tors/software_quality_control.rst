.. _software_quality_control:

Software Quality Control
------------------------

:authors: Joost Baars
:date: Mar 2020

The C++ software within this project must be checked for issues and should be 
posted online when there are no issues or warnings in the software.

The software can be checked using static code analyzers. For the C++ software, 
I've used the static code analyzer called "cppcheck". 

The compiler can check a lot of issues too. Therefore, I turn on most of the 
compiler warnings.

Cppcheck
~~~~~~~~
A static code analyzer for C++.

Install
*******
Cppcheck can be installed using the following command:

  .. code-block:: bash
  
   sudo apt-get install cppcheck

Usage
*****
The command below should be executed in the main directory of the C++ project. 
Ideally, the main directory contains all the source code. If not, ``cppcheck`` 
can only partially scan the code. This command could also be executed in a higher 
directory level containing all source code, then you should exclude all other files 
that are not the source code.

 .. code-block:: bash
  
   cppcheck --enable=all --inconclusive --std=posix -i build/ .

This scans all the files recursively from your main directory. The ``-i build/`` 
command tells cppcheck not to check the build folder (because these are not 
C/C++ files). The ``-i`` can be used for more directories. For each directory, 
an additional ``-i <file/directory name>`` tag needs to be used. So for 
multiple directories that need to be ignored, there are multiple ``-i`` tags. 

The other commands: ``--enable=all --inconclusive --std=posix`` are enabling all 
possible warnings/errors for the static code analyzer. These results can be 
ignored if you have a good reason for it. The results are in general useful and 
enhance the quality of the code.

All commands of cppcheck can be found using only the command ``cppcheck`` in the 
terminal.

Compiler warnings
~~~~~~~~~~~~~~~~~
In C++ I enable the following compiler warnings:

 .. code-block:: bash
  
   -pedantic -Wall -Wcast-align -Wcast-qual -Wdisabled-optimization 
   -Winit-self -Wmissing-declarations -Wmissing-include-dirs -Wredundant-decls 
   -Wshadow -Wsign-conversion -Wundef -Werror
   -Wempty-body -Wignored-qualifiers -Wmissing-field-initializers 
   -Wsign-compare -Wtype-limits  -Wuninitialized

This list shows most of the warnings. In most cases, warnings mean bad code. 
Therefore, one of the warnings that is enabled is ``-Werror``. This warning makes 
errors from warnings. Therefore, you must solve the warning before the code 
becomes compilable again. 

CMake
*****
In CMake, these warnings can be enabled by placing the following in the 
``CMakeLists.txt`` file:

 .. code-block:: bash

  # Add warnings
  target_compile_options(<Project Name> PRIVATE -pedantic -Wall -Wcast-align -Wcast-qual -Wdisabled-optimization 
                                               -Winit-self -Wmissing-declarations -Wmissing-include-dirs -Wredundant-decls 
                                               -Wshadow -Wsign-conversion -Wundef -Werror
                                               -Wempty-body -Wignored-qualifiers -Wmissing-field-initializers 
                                               -Wsign-compare -Wtype-limits  -Wuninitialized )

``<Project Name>`` should be the name of the project or the library.

Links
~~~~~

 * Cppcheck official website: https://sourceforge.net/projects/cppcheck/