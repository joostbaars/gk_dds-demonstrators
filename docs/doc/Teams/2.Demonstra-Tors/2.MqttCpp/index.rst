.. _mqtt_cpp:

MQTT C++
--------

:authors: Sam Laan
:date: March 2020

Description
"""""""""""
MQTT stands for Message Queuing Telemetry Transport. MQTT is a 
lightweight publish/subscribe messaging protocol\cite{MQTT:online}.
It was designed to be used with constrained devices and low-bandwidth, 
high-latency or unreliable networks.
It was developed by IBM and standardized by OASIS.
MQTT works using a client and a broker.
The client publishes messages to a topic on a broker, clients that are subscribed 
to this topic get these messages from the broker.
These messages can also be retained for future subscribers.
Clients can subscribe to multiple topics and receive every message 
published to each topic.
There are multiple MQTT clients and brokers available.

MQTT install
""""""""""""

For MQTT the Mosquitto broker is used, this broker was chosen because it has 
the best throughput, least latency and is the most popular broker.

.. code-block:: C++
   
   sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa
   sudo apt-get update
   sudo apt-get install mosquitto

For the first version paho MQTT was used, which can be installed by 
performing the following steps (check the readme of their GitHub page for updates):
firstly you need to install the following packages:

.. code-block:: C++

    sudo apt-get install build-essential gcc make cmake cmake-gui cmake-curses-gui
    sudo apt-get install libssl-dev 
    sudo apt-get install libcppunit-dev

After this the c library of paho needs to be installed:

.. code-block:: C++

    git clone https://github.com/eclipse/paho.mqtt.c.git
    cd paho.mqtt.c
    cmake -Bbuild -H. -DPAHO_WITH_SSL=ON -DPAHO_ENABLE_TESTING=OFF
    sudo cmake --build build/ --target install
    sudo ldconfig

This builds with SSL/TLS enabled. If that is not desired, omit the -DPAHO_WITH_SSL=ON.
After this, the C++ library can be installed.

.. code-block:: C++
   
    git clone https://github.com/eclipse/paho.mqtt.cpp
    cd paho.mqtt.cpp
    cmake -Bbuild -H. -DPAHO_BUILD_DOCUMENTATION=TRUE -DPAHO_BUILD_SAMPLES=TRUE
    sudo cmake --build build/ --target install
    sudo ldconfig

The library can now be used and should be linked as shown below using CMAKE:

.. code-block:: C++

    find_package(PahoMqttCpp REQUIRED)
    target_link_libraries({yourexecutable} paho-mqttpp3 paho-mqtt3as)


MQTT solutions
""""""""""""""
This list contains the solutions regarding MQTT. 

*   :ref:`mqtt_cpp_matrixboard`


Links
"""""
* List of brokers: https://github.com/mqtt/mqtt.github.io/wiki/brokers
* List of libraries: https://github.com/mqtt/mqtt.github.io/wiki/libraries
* Effects of QoS standards: https://www.researchgate.net/profile/Shinho_Lee4/publication/261230513_Correlation_analysis_of_MQTT_loss_and_delay_according_to_QoS_level/links/5dc93fcb458515143500e981/Correlation-analysis-of-MQTT-loss-and-delay-according-to-QoS-level.pdf
* Comparison brokers: http://www.shudo.net/publications/201709-CloudNet-2017-hetero-MQTT-brokers/banno-CloudNet-2017-hetero-MQTT-brokers.pdf
* Github page paho.mqtt.cpp: https://github.com/eclipse/paho.mqtt.cpp