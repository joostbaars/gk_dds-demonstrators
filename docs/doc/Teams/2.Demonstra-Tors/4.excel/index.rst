.. _excel:

Excel
------------------

:authors: Sam Laan
:date: April 2020

Description
"""""""""""
Excel is used in some cases to visualise data collected within performance  
measurements.
Used Excel features and techniques will be described.
These descriptions can be found using the links underneath.

Features and techniques
"""""""""""""""""""""""
This list contains the findings regarding the QoS policies. 

.. toctree::
    :titlesonly:
    :maxdepth: 0
    :glob:

    power_pivot