.. _NucleoSetupInitial:

#######################
How to setup the Nucleo
#######################

:authors: Furkan Ali Yurdakul
:date: March 2020

In this page, there will be a step by step tutorial to make sure
your Nucleo is set up and ready to be programmed.

For this tutorial the following products have been used:
  * Nucleo
  * Lenovo Thinkpad T470 (Windows)
  * Micro USB cable
  * STM32CubeIDE

*******************************
Install programming environment
*******************************

First, connect the Nucleo with a micro USB cable to your PC.

.. image:: NucleoSetupInitialImages/Step1.png
   :scale: 10 %
   :alt: Connect cable
   :align: center

To be able to program the Nucleo the standard programming environment is used, 
called the STM32CubeIDE. This IDE can be downloaded via this 
link: https://www.st.com/en/development-tools/stm32cubeide.html.

First, open the downloaded executable to start the installation.

.. image:: NucleoSetupInitialImages/Step2.png
   :scale: 70 %
   :alt: Start installation
   :align: center

Within the installation, it is decided to install all the components as seen below.

.. image:: NucleoSetupInitialImages/Step3.png
   :scale: 70 %
   :alt: Software components
   :align: center

The destination folder should not matter, but for this tutorial it has been chosen 
to use the default destination.

.. image:: NucleoSetupInitialImages/Step4.png
   :scale: 70 %
   :alt: Folder destination
   :align: center

If asked to install Universal Serial Bus software, click "Install".

.. image:: NucleoSetupInitialImages/Step5.png
   :scale: 70 %
   :alt: STM drivers
   :align: center

This is how it should look like when the installation of the driver is finished.

.. image:: NucleoSetupInitialImages/Step6.png
   :scale: 70 %
   :alt: Installation finished
   :align: center

If everything went as expected, continue with the next step. If not, uninstall the 
STM32Cube IDE and redo these steps.

*******************
Start a new project
*******************

After opening the IDE, the starting screen will look like the picture below.
Click on the left top button to start a new project.

.. image:: NucleoSetupInitialImages/Step7.png
   :scale: 55 %
   :alt: Start project
   :align: center

After starting a new project, the IDE will ask what the project is targeted for.
In this case, the project is targeted towards the Nucleo-F767ZI.
To target the project for this specific board, click on board selector and
search for "Nucleo-F767ZI". Thereafter click on the star next to the board 
and click finish.

.. image:: NucleoSetupInitialImages/Step8.png
   :scale: 45 %
   :alt: Target board
   :align: center

The project needs a name, any name will do. But for this project, it’s chosen 
to call it "HelloWorld" as this project will be used for more simple tests in 
the future.

For this tutorial, the options selected in the picture below will be used.

.. image:: NucleoSetupInitialImages/Step9.png
   :scale: 75 %
   :alt: Setup project
   :align: center

The setup will ask to initialize everything in its default mode.
For this tutorial, it's chosen to make use of this. In case of additional research, 
it will make it easier for the user when using the default initialization.

.. image:: NucleoSetupInitialImages/Step10.png
   :scale: 65 %
   :alt: Setup project
   :align: center

Accept opening the project in the STM32CubeMx perspective.

.. image:: NucleoSetupInitialImages/Step11.png
   :scale: 65 %
   :alt: Setup project
   :align: center

After finishing the setup, the IDE will start downloading and loading
all the needed files and it should look like the picture down below.
When this is done the IDE is finished setting up.

.. image:: NucleoSetupInitialImages/Step12.png
   :scale: 55 %
   :alt: Setup project
   :align: center

If everything went as expected, continue with the next step. If not, delete the 
project and redo this step.

**************
Update drivers
**************

When the project is finished building it is not yet able to program the Nucleo.
This is because the drivers that are installed are not up to date.
To update the drivers click on the help button and afterward click on ST-LINK 
Upgrade.

.. image:: NucleoSetupInitialImages/Step13.png
   :scale: 60 %
   :alt: ST-LINK upgrade
   :align: center

The tab that it opens requires it to be in update mode, to enable update mode 
click on the button "Open in update mode". Afterward, click on the "Upgrade" 
button to start the installation and wait until it finishes.

.. image:: NucleoSetupInitialImages/Step14.png
   :scale: 65 %
   :alt: Update mode
   :align: center

If everything went as expected, continue with the next step. If not, redo this step.

************************************
Configure pin-out for onboard button
************************************

To test if the Nucleo isn't broken and can be programmed, the onboard button(BT1) 
will be used to toggle the red LED(LD3).

The configurations of these pin-outs can be done using the IDE controller interface.
Open HelloWorld.ioc to open the interface. Afterward, click on the 
System Core category and select GPIO to find the configurations for the pin-outs.

.. image:: NucleoSetupInitialImages/Step15.png
   :scale: 65 %
   :alt: Pin configuration
   :align: center

Pin PC13 is connected to the onboard button and pin PB14 is connected 
to the red LED. Pin PB14 should already have a user label "LD3". To make 
it easier to program pin PC13, PC13 will get the user label “BT1“.

When done, save the interface via "CTRL+S" buttons and click yes to 
generate the code for it.

.. image:: NucleoSetupInitialImages/Step16.png
   :scale: 65 %
   :alt: Generate code
   :align: center

If everything went as expected, continue with the next step. If not, redo this step.

*************
Code flashing
*************

Add the following code to your while loop:

  .. code-block:: c
  
    if(HAL_GPIO_ReadPin(BT1_GPIO_Port, BT1_Pin)){
      HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_SET);
    } else {
      HAL_GPIO_WritePin(LD3_GPIO_Port, LD3_Pin, GPIO_PIN_RESET);
    }

When done adding click on Run code and wait till it's finished.
The Run code can be found on the top of the IDE see the picture below.

.. image:: NucleoSetupInitialImages/Step17.png
   :scale: 65 %
   :alt: Run code
   :align: center

If everything went as expected, test the board and continue 
with the next step. If not, delete all written code and redo this step.

**********
End result
**********

If everything went successful you should be able to click on 
the onboard button to toggle a led.

.. image:: NucleoSetupInitialImages/Step18.png
   :scale: 15 %
   :alt: End result
   :align: center

If this is not happening restart process from `Configure pin-out for onboard 
button`_. If it did work, this means your board is successfully set and is ready 
for future projects.

Check ":ref:`NucleoSetupSerial`" to learn how to make serial communication with 
the Nucleo.