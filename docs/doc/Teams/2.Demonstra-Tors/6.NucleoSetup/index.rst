.. _NucleoSetupExplanation:

############
Nucleo Setup
############

:authors: Furkan Ali Yurdakul
:date: April 2020

***********
Description
***********

The Nucleo-F767ZI is a development board that will be used to demonstrate the DDS 
implementation on embedded bare-metal hardware.

The user will be guided through some steps, to make sure the hardware is correctly 
working and the user is capable of making use of the required features. Make 
sure the Nucleo is connected to the computer, when making use of a terminal or 
flashing the code.

From now on the Nucleo-F767ZI will be shortened to Nucleo.

More information about the Nucleo can be found in the `Links`_.

*********************
Nucleo Initialization
*********************

This list contains guides to set up different parts of the Nucleo.

.. toctree::
    :titlesonly:
    :maxdepth: 0
    :glob:

    NucleoSetupInitial
    NucleoSetupSerial
    
*****
Links
*****

* Datasheet of the Nucleo: https://www.st.com/resource/en/datasheet/stm32f767zi.pdf 
* IDE used for the Nucleo: https://www.st.com/en/development-tools/stm32cubeide.html
* Source code for the guide: ``src/demonstrators/BareMetalEmbedded/HelloWorld/``