.. _NucleoDDS:

############
Nucleo DDS
############

:authors: Furkan Ali Yurdakul
:date: May 2020

***********
Description
***********

DDS is a communication that is done in Real-Time. For this reason, the Nucleo 
is required to function in Real-Time as well.

Besides being Real-Time, the communication is required to use a network 
communication protocol, for example TCP/IP. For the Nucleo, this is only possible 
via the Ethernet port. The pin-out needs to be set up for the Ethernet port as 
well as the TCP/IP stack. For the TCP/IP the build-in LwIP stack will be used, 
which is made for the embedded application on STM32 microcontrollers. On top off 
the TCP/IP stack there will be the DDS stack. For this, the Eprosima’s FastRTPS 
library is chosen.

These libraries will then be combined to create the solution for using DDS on 
embedded bare-metal hardware.

More information about the Nucleo can be found in the `Links`_.

*********
DDS Setup
*********

This list contains guides to set up different parts of the Nucleo.

.. toctree::
    :titlesonly:
    :maxdepth: 0
    :glob:

    DDSFreeRTOS
    
*****
Links
*****

* Datasheet of the Nucleo: https://www.st.com/resource/en/datasheet/stm32f767zi.pdf
* IDE used for the Nucleo: https://www.st.com/en/development-tools/stm32cubeide.html
* Manual of the IDE: https://www.st.com/resource/en/user_manual/dm00104712-stm32cubemx-for-stm32-configuration-and-initialization-c-code-generation-stmicroelectronics.pdf 
* Comparison standard and Real-Time operating system: https://www.microcontrollertips.com/real-time-standard-how-to-choose-rtos/ 
* Webpage of FreeRTOS: https://www.freertos.org/ 
* Source code for the guide: ``src/demonstrators/BareMetalEmbedded/FreeRTOS/``