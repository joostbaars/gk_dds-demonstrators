.. _python:

Round trip in Python
--------------------

Description
"""""""""""

The round trip python directory can be found in ``src/demonstrators/RoundTrip/Python/``.
This directory contains the source for the round trip and also the flood implementation.

The DDShub.py needs to be setup with a publishing and subscribing topic, acting as a hub.
The RoundTrip.py and Flood.py act as the master and do not have to be setup. Here the publishing
and subscribing topic are hard coded. So for one hub the subscribing topic and another hub the
publishing topic are also fixed, which 'connect' to the master.

To setup a round trip of a master and three hubs the following needs to be configured.

  .. code-block:: bash

     ./DDShub.py Master_Out <publish name hub1>
     ./DDShub.py <publish name hub1> <publish name hub2>
     ./DDShub.py <publish name hub2> Master_in
     ./RoundTrip.py

An example of this, for the hubs, is present in bash script run_L3.sh. 
To setup a flood the RoundTrip.py needs to be replaced with Flood.py.

Execution
~~~~~~~~~

Use DDShub.py to setup either a flood or round trip setup (see run_L3.sh for an example).
To run the flood example:

  .. code-block:: bash

   ./run_L3.sh
   ./Flood.py <number of messages to send>

   for <number of messages to send> a value can be specified. If omitteda, a value of 1000 will be used.

To run the round trip example:

  .. code-block:: bash

   ./run_L3.sh
   ./RoundTrip.py

The above will only do one run. For more accurate measurements multiple runs have to be made.
This can be achieved by using a loop (script run_RoundTrip_10times.sh). This script also logs
each output of a single run into a separate log file.

 .. code-block:: bash

    #!/bin/bash
    i=1
    while [ $i -le 10 ]
    do
       ./RoundTrip.py > ~/RoundTrip_python_$i.log
       let "i = i + 1"
    done

A similar script can also be created for executing flood 10 times, with the number of messages
as argument.

 .. code-block:: bash

    #!/bin/bash
    i=1
    while [ $i -le 10 ]
    do
       ./Flood.py $1 > ~/Flood_python_$i.log
       let "i = i + 1"
    done

Measurements
~~~~~~~~~~~~

For the measurements a setup has been chosen of four hubs of which one is the master doing
all the measurements.

The measurements for the inter process communication (IPC) of the roundtrip and flood experiments
have been gathered on a hp laptop, issued by Capgemini, and a Raspberry Pi, issued by the product
owner. All the hubs run on the same hardware, either the laptop or the Raspberry Pi.

The measurements for the inter system communication (ISC) of the round trip and flood expirements
have been gathered on both above mentioned hardware, for the IPC, and additionally an ethernet
router. Two hubs run on the laptop, the master and hub 2, and two hubs run on the Raspberry Pi,
hub 1 and hub 3. Since the communication will start at the master to hub 1, from hub 1 to hub2,
from hub 2 to hub 3 and from hub 3 to the master, there is no inter process communication on
either the laptop or the Raspberry Pi. There is only inter system communication.

Specifications of the hardware:
HP laptop: HP Elitebook 850GS, Intel i7-8650 CPU @ 1,9 GHz 2,11 GHz
Raspberry Pi: Raspberry Pi 3 Model B v1.2, Broadcomm BCM2837 CPU @ 1,2 GHz
Ethernet router: Netgear WGR614 v7

For the round trip several runs have been performed, with the averages as the following result:

+--------------------+---------------------------------------+
|                    | Round trip (ms)                       |
|                    +-------+-------+-------+-------+-------+
|                    | run 1 | run 2 | run 3 | run 4 | run 5 |
+--------------------+-------+-------+-------+-------+-------+
| HP  laptop   (IPC) | 1,357 | 1,549 | 1,534 | 1,689 | 1,484 |
+--------------------+-------+-------+-------+-------+-------+
| Raspberry Pi (IPC) | 8,623 | 8,775 | 8,705 | 8,718 | 8,574 |
+--------------------+-------+-------+-------+-------+-------+
| laptop & Pi  (ISC) | 7,691 | 7,822 | 7,825 | 7,599 | 8,126 |
+--------------------+-------+-------+-------+-------+-------+

 .. image:: MeasurementDiagrams/RoundTripPython.png
    :alt: Average time in ms for a round trip
    :align: center

For the ISC measurements the expected time for a round trip would be roughly:
"(Raspberry Pi(IPC) + HP laptop (IPC))/2", which is approximately 5 ms. However, based on the figures above
there is a difference of around 2,5 to 3,1 ms. This time difference is consumed by the router, since
this hardware has been added to the configuration.

For the flood also several runs have been performed, with the averages of all runs per different
number of messages as the following result:
(The time measured is the time it takes from sending the first message and receiving the last message)

+--------------------+-----------------------------+
|                    | Flood (s), x 1000 messages  |
|                    +-----+------+-------+--------+
|                    |  1  |  10  |  100  |  1000  |
+--------------------+-----+------+-------+--------+
| HP laptop    (IPC) | 0,6 |  4,6 |  56,1 |  578,5 |
+--------------------+-----+------+-------+--------+
| Raspberry Pi (IPC) | 3,2 | 31,6 | 321,5 | 3480,1 |
+--------------------+-----+------+-------+--------+
| laptop & Pi  (ISC) | 2,7 | 24,7 | 235,5 | 2291,7 |
+--------------------+-----+------+-------+--------+

 .. image:: MeasurementDiagrams/FloodPython.png
    :alt: Average time in s for a # of messages (1000, 10.000, 100.000, 1.000.000)
    :align: center

From the flood measurements a linear pattern can be deducted for both hardware (IPC), although
this was not expected for the runs on the Raspberry Pi, which has no active cooling system. The
same linear pattern can also be seen for the ISC measurement.
