ZMQ
-----------

:authors: Sam Laan
:date: March 2020

Description
"""""""""""
The problem description for the matrixboard can be found at: :ref:`MBC`.

Dynamic discovery problem
"""""""""""""""""""""""""
.. note:: From the ZeroMQ guide (http://zguide.zeromq.org/page:all)

One of the problems you will hit as you design 
larger distributed architectures is discovery. 
That is, how do pieces know about each other? It's especially difficult 
if pieces come and go, so we call this the "dynamic discovery problem".

There are several solutions to dynamic discovery. 
The simplest is to entirely avoid it by hard-coding (or configuring) 
the network architecture so discovery is done by hand.
That is, when you add a new piece, you reconfigure the network to know about it.
In practice, this leads to increasingly fragile and unwieldy architectures. 
Let's say you have one publisher and a hundred subscribers. 
You connect each subscriber to the publisher by configuring a publisher 
endpoint in each subscriber. 
That's easy. Subscribers are dynamic; the publisher is static. 
Now say you add more publishers. Suddenly, it's not so easy any more. 
If you continue to connect each subscriber to each publisher, 
the cost of avoiding dynamic discovery gets higher and higher.

The very simplest answer is to add an intermediary.
A static point in the network to which all other nodes connect.
In classic messaging, this is the job of the message broker. 
ZeroMQ doesn't come with a message broker as such, but it lets us build 
intermediaries quite easily.
It's better to think of intermediaries as simple stateless message switches. 
A good analogy is an HTTP proxy; it's there but doesn't have any special role. 
Adding a pub-sub proxy solves the dynamic discovery problem in our example. 
We set the proxy in the "middle" of the network. 
The proxy opens an XSUB socket, an XPUB socket, 
and binds each to well-known IP addresses and ports. 
Then, all other processes connect to the proxy, instead of to each other. 
It becomes trivial to add more subscribers or publishers.

.. image:: zmq_proxy_arch.png
   :width: 300
   :align: center

We need XPUB and XSUB sockets because ZeroMQ does 
subscription forwarding from subscribers to publishers. 
XSUB and XPUB are exactly like SUB and PUB except they 
expose subscriptions as special messages. 
The proxy has to forward these subscription messages from subscriber 
side to publisher side, 
by reading them from the XPUB socket and writing them to the XSUB socket. 
This is the main use case for XSUB and XPUB.

Implementation
""""""""""""""
There are different implementations of the zmq matrixboard solution. 
A short description is made for the execution of a particular implementation. 
This description contains information about 
how the implementation can be compiled and executed. 


This list contains the different implementations:

.. toctree::
    :titlesonly:
    :maxdepth: 1
    :glob:

    cpp
