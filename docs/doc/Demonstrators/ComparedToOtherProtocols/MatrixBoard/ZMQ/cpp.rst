.. _zmq_cpp_matrixboard:

ZMQ C++ Matrixboard
-------------------

:authors: Sam Laan
:date: March 2020

Description
"""""""""""
This page contains the information needed to run the ZMQ C++ matrixboard 
solution.
It is assumed that you have already installed the zeromq C and C++ libs.
If not please refer to the ZMQ C++ page(:ref:`zmq_cpp`).

How to run
"""""""""""
.. note:: For collecting measurements
    The collection of measurement data is built within the application.
    It is stored in a .csv file.
    To create this file please refer to `Measurement file`_

To run the program navigate to the 
/src/demonstrators/ComparedToOtherProtocols/ZMQ/MatrixBoard/C++ 
folder of the repository.
The source code and CMakeLists of the program are in this folder.
Now to build the solution perform the following commands:

.. code-block:: C++
   
   mkdir build
   cd build
   cmake ..
   make -j4

Two executables called "zmqmatrixboard" and "matrixboardproxy" 
should have appeared in the build folder.
These can be started using the following commands:

.. code-block:: C++
   
   ./matrixboardproxy
   ./zmqmatrixboard

The matrixboardproxy will show on which ports it runs.
The proxy is necessary for discovery, more on that in the section below.
The zmqmatrixboard program will ask for a number.
This number can range from 1 till 9.
This number will be the id of the matrixboard and 
will determine on which port the matrixboard publishes.

Performance
"""""""""""
The collection of performance measurements is built into the application itself.
It writes the time it sends and the time it receives messages to a CSV file.
This CSV file can be used to visualise the data using various tools to do so.
In this case, Excel has been used. The choice for Excel was made because it 
is a commonly known and used program and it is 
installed on a Capgemini/Sogeti laptop by default.
The processed_measuremens.xlsx file shows an example of how to visualise 
the CSV data using Excel and Power Pivot.
More on Excel and Power Pivot can be found here :ref:`power_pivot`.

Findings
""""""""
The performance test was executed with 3 matrixboards 
and resulted in the following graphs(time is measured in ms).

.. image:: zmq_deliverytime.PNG
   :width: 800
   :align: center

The graph shows that the delivery time steadily increases.
This is because the application can send messages faster than 
it can process them.
This results in a message queue which keeps on growing.
The last message was not received because the application was stopped 
before it could receive it. This explains the drop in the graph at the last message.

Measurement file
""""""""""""""""
The file in which the measurements are stored in CSV format is called measurements.csv.
An executable is built during the 'make' process to create this CSV file in correspondence with 
the data that will be stored in it.
This executable deletes any existing file called measurements.csv in the build folder 
and creates a new one containing the data headers only.
To start the executable run the following command:

.. code-block:: C++

    ./measurementsfiles
