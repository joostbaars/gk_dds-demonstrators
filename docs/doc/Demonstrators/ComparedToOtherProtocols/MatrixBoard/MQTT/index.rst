MQTT
----

Description
"""""""""""

The problem description for the matrixboard can be found at: :ref:`MBC`.

Implementation
""""""""""""""

There are different implementations of the MQTT matrixboard solution. 
A short description is made for the execution of a particular implementation. 
This description contains information about how the implementation can be compiled and executed. 

This list contains the different implementations:

.. toctree::
    :titlesonly:
    :maxdepth: 1
    :glob:

    cpp