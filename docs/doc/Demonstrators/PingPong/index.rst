PingPong
---------

Description
"""""""""""
Ping Pong is a way to test connectivity between two computers or programs.
It works by sending a message from one entity to another.
This other entity will reply on the first message and so on.
The figure below visualises this concept.
.. uml::

   @startuml
   ping -right-|> pong : message
   pong -left-|> ping : message 
   @enduml


Implementation
""""""""""""""
There are different implementations of the PingPong. 
A short description is made for the execution of a particular implementation. 
This description contains information about how the implementation can be compiled and executed. 

This list contains the different implementations:

.. toctree::
    :titlesonly:
    :maxdepth: 1
    :glob:

    cpp
    python

